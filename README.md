# Distributed-Key-Value-Store
A simple `scalable`, `reliable`, `sequentially consistent` key value store based on chord protocol using RPC.
It offers failure transparency
Tested manually for simple test cases.

## Throughput
900 Writes/s
19000 Reads/s
with 6 nodes running on 4 core 2.4GHz and 3.7 GiB RAM system

## Usage
use the following commands to run the CHORD distributed key value store:
```
> cd node
> go build
> ./node ip-address:port initonly
> ./node ip-address:port join-node-ip-address:port
```

to run a client:
```
> cd client
> go build
> ./client
```
commands:
```
node.write key-here value-here
node.read key-here
```
