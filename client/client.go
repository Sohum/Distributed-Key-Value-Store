package main

import (
	"bufio"
	"fmt"
	"net/rpc"
	"os"
	"strings"

	"utils"
)

func main() {
	fmt.Print(">>> Enter all node IPs:\n>>>")
	var scanner *bufio.Scanner = bufio.NewScanner(os.Stdin)

	scanner.Scan()
	var userInput string = scanner.Text()
	var nodeIps []string = strings.Fields(userInput)

	fmt.Print(">>> Enter command node.read|node.write:\n>>> ")
	for scanner.Scan() {
		var dummy int
		var value string
		var client *rpc.Client
		var err error

		var userInput []string = append(strings.Fields(scanner.Text()), "nil")
		var command string = userInput[0]
		if len(userInput) > 1 {
			userInput = userInput[1:len(userInput)-1]
		}

		var randomIndex int = utils.RandomInt(0, len(nodeIps))
		if client, err = rpc.DialHTTP("tcp", nodeIps[randomIndex]); err != nil {
			fmt.Println(err)
			continue
		} else {
			fmt.Printf("Connected to node=%s\n", nodeIps[randomIndex])
		}

		switch command {
		case "node.write":
			if len(userInput) != 2 {
				fmt.Printf("Unexpected format: please pass `node.write key value`; passed `node.write %s`\n", userInput)
			} else if err = client.Call("Node.Write", userInput, &dummy); err == nil {
				fmt.Printf("node.write completed successful. node called=%s\n", nodeIps[randomIndex])
			} else {
				fmt.Println(err)
			}

			break
		case "node.read":
			if len(userInput) != 1 {
				fmt.Printf("Unexpected format: please pass `node.read key`; passed `node.read %s`\n", userInput)
			} else if err = client.Call("Node.Read", userInput[0], &value); err == nil {
				fmt.Printf("node.read completed successful. node called=%s. result = %s\n", nodeIps[randomIndex], value)
			} else {
				fmt.Println(err)
			}

			break
		case "exit":
			fmt.Println("exit: exiting the process ...")
			return
		case "node.delete":
		default:
			fmt.Println("Not supported yet")
		}
		fmt.Print(">>> Enter command node.read|node.write:\n>>> ")
	}
}
