package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"utils"
)

type UserCommand struct {
	Address string
	CommandRoot string
	Params []interface{}
	UnpreparedParams []string
	IsExecutable bool
}

func (command *UserCommand) ParseFrom(input string) bool {
	var userCommand = strings.Fields(input)
	if len(userCommand) < 2 {
		fmt.Println("Invalid input format. Sample=<:8000 Node.Read key> (w/o <, >)")
		return false
	}

	command.Address = userCommand[0]
	command.CommandRoot = "Node." + strings.ToUpper(userCommand[1][:1]) + strings.ToLower(userCommand[1][1:])
	command.UnpreparedParams = userCommand[2:]
	command.PrepareParams()
	return true
}

func (command *UserCommand) PrepareParams() {
	switch command.CommandRoot {
	case "Node.Read":
		var value string
		command.Params = append(command.Params, command.UnpreparedParams[0])
		command.Params = append(command.Params, &value)
		command.IsExecutable = true
		break
	case "Node.Write":
		var output int
		command.Params = append(command.Params, command.UnpreparedParams)
		command.Params = append(command.Params, &output)
		command.IsExecutable = true
		break
	}
}

func (command *UserCommand) Execute() {
	if command.IsExecutable {
		var err error = utils.Rpc(command.Address, command.CommandRoot, command.Params[0], command.Params[1])
		if err == nil {
			switch command.Params[1].(type) {
			case *string:
				fmt.Println("Successfully executed fn=", command.CommandRoot, "output=", *command.Params[1].(*string))
			default:
				fmt.Println("Successfully executed fn=", command.CommandRoot, "output=", command.Params[1])
			}
		} else {
			fmt.Println("Error occured while executing fn=", command.CommandRoot, "err=", err)		
		}		
	} else {
		fmt.Println("Error occurred when trying to execute command=", command.CommandRoot, "; supported commands are [read, write]")
	}
}

func notmain() {
	fmt.Println("Starting Manual Testing Terminal ... :)")
	for true {
		scanner := bufio.NewScanner(os.Stdin)
		fmt.Print("> ")
		for scanner.Scan() {
			var userInput = scanner.Text()
			var userCommand UserCommand
			if strings.Compare(strings.ToLower(userInput), "exit") == 0 {
				fmt.Println("Terminating Manual Testing Terminal ...")
				os.Exit(0)
			} else if userCommand.ParseFrom(userInput) {
				userCommand.Execute()
			}
			fmt.Print("> ")
		}
	}
}



		// switch command.CommandRoot {
		// case "leave":

		// 	if len(userCommand) != 2 {
		// 		fmt.Println("Wrong format")
		// 		continue
		// 	}

		// 	var dummy string 
		// 	err = client.Call("Node.DoLeave", dummy, &dummy)

		// 	if err != nil {
		// 		fmt.Println(err)
		// 	} else {
		// 		fmt.Println("Node Leave Successful")
		// 	}
		// case "del":

		// 	if len(userCommand) != 3 {
		// 		fmt.Println("Wrong format")
		// 		continue
		// 	}

		// 	var dummy int 
		// 	err = client.Call("Node.DeleteKey", userCommand[2], &dummy)

		// 	if err != nil {
		// 		fmt.Println(err)
		// 	} else {
		// 		fmt.Println("Delete Successful")
		// 	}
		// case "stat":
		// 	if len(userCommand) != 2 {
		// 		fmt.Println("Wrong format")
		// 		continue
		// 	}

		// 	var dummy int 
		// 	var stat Stat
		// 	err = client.Call("Node.GetStat", dummy, &stat)

		// 	if err != nil {
		// 		fmt.Println(err)
		// 	} else {

		// 		fmt.Println("Stats {Qsize:",stat.Qsize,", KVSsize:",stat.KVSsize,", StartRange:",stat.StartRange,", EndRange:",stat.EndRange,"}")
		// 	}		
		//}