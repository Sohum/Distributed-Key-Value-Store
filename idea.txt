How Sequrntial Consistency? => Assuming a process is a session and that there is TO broadcast, since replicas are guaranteed to be sequentailly consistent our system gives Sequentuial Consistent view to the User/Process.  

Update idea:
If am primary update, Else send to primary.

Read idea:
If have the key-space charge return value. Else ip=LookupIp() and LookUp() on ip.

Failure Detection idea:
1. Check with successor.
2. Check its successors. If successor dies init recovery and update successors.
Eventually the information of some dead successor reaches as successors get updated. Primary takes care of the its k-replicas. Hold the nodelock and queuelock. (Don't release and reqquire nodelock after queuelock to avoid deadlock and ensure that the next update reaches updated successors?)

UpdateSuccessors() is atomic and each phase of periodicUpdater()[only Recovery part, successor-update accounts for node-join separately, i.e., If we detect nodejoin after rpc-call we break-loop, if we have i>0 and we detect node-join we reset i=0 ] is atomic(and blocking) using nodeupdatelock.
CASE: node-join just after successor-update call

In UpdateSuccessors() we can flush KVS to successors as no updates from primary(us now, since hold lock and StartRange and Predecessors not updated untill KVS-update)
In periodicUpdater() we flush the KVS to new successors as no old-write-messages were sent to them and their KVS can be uptodate. Release the queuelock after updating the successors so that they receive new write-update messages.
In Join() all the primaries send their most recent copies to the new Successor and the KVS of newdnode is made uptodate(Sequentially consistent). Also, update MsgQueue of newnode (for T.O.), LookUp, Delete/UpdateKey work fine if replicationFActor>1
CASE: for Sequential Consistency in failures.

In UpdateSuccessors() we hold nodeupdatelock to prevent updation of successors since we release nodelock and periodicUpdater() may update successors then. 
UpdateSuccessors(..) is atomic and so is Recovery in periodicUpdater(), hence only correct successors get the new KVSs due to predecessors or the old successors get the KVSs and the new successors get the complete-new-KVS.
If periodicUpdater() updates successors before us, ok. Else if it updates in middle we deliver KVS to old succ (w/o that part), new ones get complete anyway.
CASE: predecessor and successor failure.

In periodicUpdater() we hold the nodeupdatelock to ensure we send consistent StartRange and EndRange values and we break if nodejoin occured and we only knew after UpdateSuccessors(..). We work with new successors in next iteration. If nodejoin occured we reset i = 0
CASE: Failure of an old successor,periodicUpdater(), during node-join,Join(..)


Extending Range to contin replica data as well:
Read and writes handled as they are redirected to successor for node join or failed / redirected to correct predecessor/successor in case of failed node. Always check if in range and call recursively to lookUp ip if not. Update/Read executed only if key is in our own/full range.

TODO: 2PC idea. 
CASE: predecessor dies and during recovery node fails then predecessors of node and hence its successors are not updated. Later failure detected on successor recovers. (predecessor KVS replication)
CASE:  (node replication point) node not updated with new successor, but successor is/ node fails during node-KVS-replication. UpdateSuccessor() send complete KVS of dead predecessors and periodicUpdater removes the old successor targets from msgQueue to send latest KVS copy to all (no updates from dead predecessors)

Throughput:
900 Writes/s
19000 Reads/s

Consistency Check passed.