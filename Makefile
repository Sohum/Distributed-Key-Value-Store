
all:	chord	client

phony:
	chord	client

node:	src/node.go src/node_helper.go src/common.go src/store.go utils/utils.go constants/constants.go src/main.go
	cd node; go build; cd ..

client:	client/client.go
	cd client; go build; cd ..

clean:
	rm main/node client/client