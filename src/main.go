package chord

import (
	"log"
	"net"
	"net/http"
	"net/rpc"
	"os"
	"strings"
	"sync"

	"constants"
	"utils"
)

func Main() {
	node := new(Node)
	node.Address = os.Args[1]
	node.NodeId = utils.Hash(os.Args[1])
	node.nodelock = sync.Mutex{}
	node.queuelock = sync.Mutex{}
	node.Store.MidToClkMap = make(map[Mid3Phase]Clock3Phase)
	node.Store.KeyValueStore = make(map[string]string)
	node.Store.CommitedMessageQueue.Init()
	node.Store.AbortedMessageQueue.Init()
	node.logger = log.New(os.Stderr, "", log.Ltime|log.Lmicroseconds) //Lshortfile to print line no

	node.logger.Printf("main: starting node join for node=%d, passed-node=%s(%d) ...\n", node.NodeId, os.Args[2], utils.Hash(os.Args[2]))

	rpc.Register(node)
	rpc.HandleHTTP()
	listner, err := net.Listen("tcp", os.Args[1])
	if err != nil {
		node.logger.Fatal("Node: Error occurred when attempting to start node server. Error=", err)
	}

	node.logger.Println("Node: Start listening for requests ...")
	go http.Serve(listner, nil)

	var nodeBasicInfo NodeBasicInfo
	if strings.Compare(os.Args[2], constants.InitOnly) == 0 {
		// Only Node creation is needed - no need to do <see cref="node.join" />
		nodeBasicInfo.NodeId = node.NodeId
		nodeBasicInfo.Address = node.Address
		err = node.join(nodeBasicInfo)
	} else {
		var nodeBasicInfo NodeBasicInfo
		err = utils.Rpc(os.Args[2], "Node.Successor", utils.Hash(os.Args[2]), &nodeBasicInfo)
		node.logger.Println("main: node successor info retrieved")
		if err == nil {
			err = node.join(nodeBasicInfo)
		}
	}

	if err != nil {
		node.logger.Fatal("Node: Unable to join!", err)
	}

	node.logger.Println("Node: Key Store initialized...")

	utils.SchedulePeriodicJob(func() error { node.stabilize(); return nil }, constants.StabilizePeriod)
}
