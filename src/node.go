package chord

import (
	"errors"
	"log"
	"sync"

	"constants"
	"utils"
)

type NodeIdInfo struct {
	NodeId uint64
	Address string
}

type NodeBasicInfo struct {
	NodeId uint64
	Address string
	Predecessor *NodeIdInfo
	Successors []NodeIdInfo

	// for failure recovery
	IsDead bool
}

type Node struct {
	NodeId uint64
	Address string

	FingerTable []NodeBasicInfo
	Successors []NodeBasicInfo
	NodePredecessor *NodeBasicInfo
	
	Store DistributedSharedStore
	
	nodejoined bool
	nodelock, queuelock sync.Mutex
	logger *log.Logger
}



/// <see ref=chord.Chord.GetClosestPreceedingFinger>
func (node *Node) GetClosestPreceedingFinger(key uint64, finger *NodeBasicInfo) error {
	// search in the finger table for the closest finger to `key` from farthest to closest to `node`

	// node.logger.Printf("node.GetClosestPreceedingFinger: starting key=%d ...\n", key);

	// node.prettyPrint();

	node.nodelock.Lock()
	for m := constants.FingerTableSize-1; m >= 0; m-- {
		if utils.InIntervalOO(node.FingerTable[m].NodeId, node.NodeId, key) {
			finger.NodeId = node.FingerTable[m].NodeId
			finger.Address = node.FingerTable[m].Address
			finger.Predecessor = node.FingerTable[m].Predecessor
			finger.Successors = make([]NodeIdInfo, len(node.FingerTable[m].Successors))
			copy(finger.Successors, node.FingerTable[m].Successors)
			node.nodelock.Unlock()

			// node.logger.Println("node.GetClosestPreceedingFinger: found in FT exiting ...");

			return  nil
		}
	}

	// no appropriate finger found - return `node`

	// *****************************************************************************************************
	// *****************************************************************************************************
	// ******** Special case if no appropriate finger found attempt to return predecessor for       ******** 
	// ******** `key`` == `node.NodeId` 													        ********  
	// *****************************************************************************************************
	// *****************************************************************************************************

	if node.NodeId == key && node.NodePredecessor != nil {
		finger.Address = node.NodePredecessor.Address
		finger.NodeId = node.NodePredecessor.NodeId
		finger.Predecessor = node.NodePredecessor.Predecessor
		finger.Successors = make([]NodeIdInfo, len(node.NodePredecessor.Successors))
		copy(finger.Successors, node.NodePredecessor.Successors)
		node.nodelock.Unlock()
	} else {
		finger.Address = node.Address
		finger.NodeId = node.NodeId
		node.copyPredecessorInto(finger)
		node.copySuccessorsInto(finger)
		node.nodelock.Unlock()
	}

	// node.logger.Println("node.GetClosestPreceedingFinger: returning node exiting ...");

	return nil
}



/// <see ref=chord.Chord.Predecessor>
func (node *Node) Predecessor(key uint64, predecessorInfo *NodeBasicInfo) error {

	node.logger.Printf("node.Predecessor: starting key=%d ...\n", key)

	predecessorInfo.NodeId = node.NodeId
	predecessorInfo.Address = node.Address
	node.nodelock.Lock()
	node.copyPredecessorInto(predecessorInfo)
	node.copySuccessorsInto(predecessorInfo)
	node.nodelock.Unlock()

	var err error = nil
	for err == nil && len(predecessorInfo.Successors) > 0 && !utils.InIntervalOC(key, predecessorInfo.NodeId, predecessorInfo.Successors[0].NodeId) {
		// don't make RPC call to local node
		// TODO (sodhar): can we get stuck in loop?
		node.logger.Printf("node.Predecessor: calling closest preceeding finger key=%d, predecessorInfo.NodeId=%d, predecessorInfo.Successors[0]=%d ...\n", key, predecessorInfo.NodeId, predecessorInfo.Successors[0].NodeId)
		err = utils.RpcHandleLocal(
			predecessorInfo.Address,
			"Node.GetClosestPreceedingFinger",
			key,
			predecessorInfo,
			node.Address,																	// localIpAddress
			func() error { return node.GetClosestPreceedingFinger(key, predecessorInfo) })	// localCall

		// *****************************************************************************************************
		// *****************************************************************************************************
		// ******** Special case throw if loop detected											        ********
		// *****************************************************************************************************
		// *****************************************************************************************************
		if predecessorInfo.NodeId == node.NodeId {
			return errors.New("[Node.Predecessor] Loop detected with GetClosestPreceedingFinger!")
		}
	}

	node.logger.Println("node.Predecessor: exiting ...")
	// node.prettyPrint()
	return err
}



/// <see ref=chord.Chord.Successor>
func (node *Node) Successor(key uint64, successorInfo *NodeBasicInfo) error {

	node.logger.Printf("node.Successor: starting key=%d ...\n", key)

	// speacial case: useful to fetch full NodeBasicInfo
	successorInfo.Address = node.Address
	successorInfo.NodeId = node.NodeId
	if key == node.NodeId {
		node.nodelock.Lock()
		node.copyPredecessorInto(successorInfo)
		node.copySuccessorsInto(successorInfo)
		node.nodelock.Unlock()

		node.logger.Printf("node.Successor: exiting special case ...\n")
		// node.prettyPrint()
		return nil
	}

	node.logger.Printf("node.Successor: calling node.Predecessor key=%d ...\n", key)

	// generic case: p=FindPredecessor(id); return p.Successor; 
	var predecessorInfo NodeBasicInfo
	var err error = node.Predecessor(key, &predecessorInfo)

	if err == nil {
		if (len(predecessorInfo.Successors) > 0) {
			// if we do not have successor info, fallback to node's info
			// e.g. if `node` is the only node in the ring
			// 		or if there is a goof up
			successorInfo.NodeId = predecessorInfo.Successors[0].NodeId
			successorInfo.Address = predecessorInfo.Successors[0].Address
		}

		node.logger.Printf("node.Successor: fetching full NodeBasicInfo for node=%d ...\n", successorInfo.NodeId)

		// fetch full NodeBasicInfo for successInfo
		// don't make RPC call to local node
		err = utils.RpcHandleLocal(
			successorInfo.Address,
			"Node.Successor",
			successorInfo.NodeId,
			successorInfo,
			node.Address,														// localIpAddress
			func() error { return node.Successor(node.NodeId, successorInfo) })	// localCall
	}
	
	node.logger.Printf("node.Successor: exiting ...\n")

	// node.prettyPrint()
	return err
}






/// <see ref=chord.Chord.stabilize>
func (node *Node) stabilize() {

	// Ping for HealthCheck and Update Current Successors, Predecessor information
	node.logger.Println("node.stabilize: starting ...")
	// node.prettyPrint()

	node.nodelock.Lock()
	var successor NodeBasicInfo
	var successors = make([]NodeBasicInfo, len(node.Successors))
	copy(successors, node.Successors)
	node.nodelock.Unlock()

	for i := 0; i < len(successors); i++ {		
		var err = utils.RpcWithBackoff(successors[i].Address, "Node.Successor", successors[i].NodeId, &successor)
		if err != nil {
			// remove Successor[i] which missed the heartbeat check
			node.logger.Printf("node.stabilize: remove successor=%s..., err=%s\n", successors[i].Address, err)
			node.nodelock.Lock()
			node.Successors = RemoveFromList(node.Successors, i)
			if i == 0 {
				// node.Successors[0] == node.FingerTable[0]
				if len(node.Successors) > 0 {
					node.FingerTable[0] = node.Successors[0]
				} else {
					node.FingerTable[0].NodeId = node.NodeId
					node.FingerTable[0].Address = node.Address
					node.FingerTable[0].Successors = make([]NodeIdInfo, 0)
					node.FingerTable[0].Predecessor = nil
				}
			}
			node.nodelock.Unlock()
		}
	}

	node.nodelock.Lock()
	var predecessor NodeBasicInfo
	if node.NodePredecessor != nil {
		predecessor = *node.NodePredecessor
	}
	node.nodelock.Unlock()

	if predecessor.NodeId != 0 {
		if err := utils.RpcWithBackoff(predecessor.Address, "Node.Successor", predecessor.NodeId, &successor); err != nil {
			// TODO (sodhar): recover NodePredecessor on error
			node.logger.Printf("node.stabilize: remove predecessor=%s..., err=%s", predecessor.Address, err)
			node.nodelock.Lock()
			node.NodePredecessor = nil
			node.nodelock.Unlock()
		}
	}

	node.logger.Println("node.stabilize: successors, predecessor ping done ...")

	// Find Newly Joined Successors information

	node.nodelock.Lock()
	successors = make([]NodeBasicInfo, len(node.Successors))
	copy(successors, node.Successors)
	node.nodelock.Unlock()

	var nodeInfo NodeBasicInfo
	for i := 0; i < len(successors); i++ {
		var err = utils.Rpc(successors[i].Address, "Node.Predecessor", successors[i].NodeId, &predecessor)
		if err == nil {
			if utils.InIntervalOO(predecessor.NodeId, node.NodeId, successors[i].NodeId)  && (i == 0 || utils.InIntervalOO(successors[i-1].NodeId, node.NodeId, predecessor.NodeId)) {
				// insert predecessor at index i in the successors
				node.nodelock.Lock()
				node.Successors = InsertToList(node.Successors, predecessor, i, constants.ReplicationFactor)
				if i == 0 {
					// node.Successors[0] == node.FingerTable[0]
					node.FingerTable[0] = predecessor
				}
				node.nodelock.Unlock()
				successors = InsertToList(successors, predecessor, i, constants.ReplicationFactor)
				// TODO(sodhar): NOTIFY OR MARK NEW SUCCESSOR AS NOT READY
			}

			nodeInfo.Address = node.Address
			nodeInfo.NodeId = node.NodeId
			node.copySuccessorsInto(&nodeInfo)
			node.copyPredecessorInto(&nodeInfo)
			err = utils.RpcCallAction(successors[i].Address, "Node.Notify", &nodeInfo)
		}
	}

	node.logger.Println("node.stabilize: update newly joined successor done ...")

	// Fill Unfilled Successors on Node Drop
	node.nodelock.Lock()
	if len(node.Successors) < constants.ReplicationFactor  && len(node.Successors) > 0 {
		nodeInfo = node.Successors[len(node.Successors)-1]
		node.nodelock.Unlock()
		// neat trick to get the successor of a node
		var err = utils.RpcWithBackoff(nodeInfo.Address, "Node.Successors", (nodeInfo.NodeId+1)%constants.Mod, &successor)
		node.nodelock.Lock()
		if err == nil && successor.NodeId != node.NodeId {
			// insert new valid successor at the end
			node.Successors = InsertToList(node.Successors, successor, len(node.Successors), constants.ReplicationFactor)
		}
	}
	node.nodelock.Unlock()

	node.logger.Println("node.stabilize: extend successor on node drop done ...")

	/// <see ref=chord.Chord.fixFingers>
	for i := 1; i < constants.FingerTableSize; i++ {
		var fingerIndex int = i // utils.RandomInt(1,constants.FingerTableSize-1) // skips 0
		var fingerInfo NodeBasicInfo
		var err error = node.Successor((node.NodeId+utils.Power2(fingerIndex))%constants.Mod, &fingerInfo)
		if err == nil {
			node.nodelock.Lock()
			node.FingerTable[fingerIndex] = fingerInfo
			node.nodelock.Unlock()
		}
	}  
	
	// node.prettyPrint()

	node.logger.Println("node.stabilize: random update finger table update done ...")
}


/// <see ref=chord.Chord.Notify>
func (node *Node) Notify(caller NodeBasicInfo, output *int) error {
	node.nodelock.Lock()
	if node.NodePredecessor == nil || utils.InIntervalOO(caller.NodeId, node.NodePredecessor.NodeId, node.NodeId) {
		// update with first or better predecessor information
		node.NodePredecessor = new(NodeBasicInfo)
		node.NodePredecessor.NodeId = caller.NodeId
		node.NodePredecessor.Address = caller.Address
		node.NodePredecessor.Predecessor = caller.Predecessor
		node.NodePredecessor.Successors = make([]NodeIdInfo, len(caller.Successors))
		copy(node.NodePredecessor.Successors, caller.Successors)
	}
	node.nodelock.Unlock()
	
	return nil
}



// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// func (node *Node) lookUpFingerTable(key uint64) string {

// 	// fmt.Println("lookUpFingerTable called with key =", key)
// 	var target uint64

// 	node.nodelock.Lock()
// 	if key > node.NodeId {
// 		target = uint64(math.Log2(float64(key - node.NodeId)))
// 	} else {
// 		target = uint64(math.Log2(float64(key + Power2(32) - node.NodeId)))
// 	}
	
// 	var targetLookUp string

// 	// fmt.Println("Found the location for next hop =", node.FingerTable[target],"\nWill contact it")
// 	finger := node.FingerTable[target]
// 	node.nodelock.Unlock()
// 	// node.printFingerTable()
// 	// node.printDetails()

// 	client, err := getClient(finger)

// 	if err == nil {
// 		err = client.Call("Node.IpLookUp", key, &targetLookUp)
// 		client.Close()
// 	}
	
// 	for err != nil {
// 		target = (target - 1 + 32) % 32
// 		// fmt.Println("Problem contacting it.\nFound the location for previous best hop =", node.FingerTable[target],"\nWill contact it")
// 		node.nodelock.Lock()
// 		finger = node.FingerTable[target]
// 		node.nodelock.Unlock()
		
// 		client, err = getClient(finger)
// 		if err == nil {
// 			err = client.Call("Node.IpLookUp", key, &targetLookUp)
// 			client.Close()
// 		}
// 	}
// 	// fmt.Println("Final destination for", key, "was found to be at", targetLookUp)
// 	return targetLookUp
// }

// func (node *Node) IpLookUp (key uint64, addr *string) error {
// 	// node.logger.Printf("f=IpLoockup, key=%d\n",key)	
// 	// fmt.Println("IpLookUp called with key =", key)
// 	node.nodelock.Lock()
// 	if node.inRange(key)==0 {
// 		node.nodelock.Unlock()
// 		*addr = node.Address 
// 	} else{
// 		node.nodelock.Unlock()
// 		*addr = node.lookUpFingerTable(key)
// 	}
// 	// fmt.Println("IpLookUp returns the destination =", *addr)
// 	return nil
// }

// func (node *Node) LookUp(key string, value *string) error {
// 	// node.logger.Printf("f=LookUp, key=%s\n",key)	
// 	// fmt.Println("LookUp called for key =", key)
// 	var err error
// 	var client *rpc.Client
// 	err = nil
// 	hash := utils.Hash(key)

// 	node.nodelock.Lock()
// 	if node.inRange(hash)==0 {
// 		*value = node.KeyValueStore[key]
// 		node.nodelock.Unlock()
// 		// fmt.Println("Lookup resulted into value =", *value)
// 	} else {
// 		node.nodelock.Unlock()
// 		var targetIp string
// 		node.IpLookUp(hash, &targetIp)
// 		client, err = getClient(targetIp)
// 		if err == nil{
// 			err = client.Call("Node.LookUp", key, value)
// 			client.Close()
// 		}
// 	}

// 	return err
// }

// func (node *Node) ForceUpdateKey(keyValue []string, dummy *int) error {	
// 	node.nodelock.Lock()
// 	node.logger.Printf("f=ForceUpdateKey, key=%s, value=%s\n",keyValue[0],keyValue[1])
// 	// fmt.Println("(Replication) Added key =", keyValue[0], ", value =", keyValue[1])
// 	node.KeyValueStore[keyValue[0]] = keyValue[1]
// 	node.nodelock.Unlock()

// 	return nil
// }

// func (node *Node) UpdateKey(keyValue []string, dummy *int) error {
// 	// fmt.Println("Add request came for key =", keyValue[0], ", value =", keyValue[1])
// 	var err error
// 	var client *rpc.Client
// 	err = nil

// 	hash := utils.Hash(keyValue[0])

// 	node.nodelock.Lock()
// 	if index := node.inRange(hash); index>=0 {
// 		if index==0 {
// 			node.logger.Printf("f=UpdateKey, key=%s, value=%s\n",keyValue[0],keyValue[1])
// 			node.KeyValueStore[keyValue[0]] = keyValue[1]

// 			var msg Msg;
// 			msg.Msg = make([]string,2)
// 			copy(msg.Msg,keyValue)
			
// 			msg.Address = node.Address
// 			node.queuelock.Lock()
// 			for i:=0 ; i< constants.ReplicationFactor-1 ;i++ {
// 				if strings.Compare(node.Successors[i],node.Address) == 0 {
// 					break
// 				}
// 				msg.Address = node.Successors[i]
// 				node.MsgQueue.pushBack(msg)
// 			}
// 			node.queuelock.Unlock()
// 			node.nodelock.Unlock()
// 		} else {
// 			primaryIp := node.Predecessors[index-1].Address
// 			node.nodelock.Unlock()
// 			client, err = getClient(primaryIp)
// 			if err == nil {
// 				err = client.Call("Node.UpdateKey", keyValue , dummy)
// 				client.Close()
// 			}
// 		}	
// 	} else {
// 		node.nodelock.Unlock()
// 		var targetIp string
// 		node.IpLookUp(hash, &targetIp)
// 		client, err = getClient(targetIp)
// 		if err == nil{
// 			err = client.Call("Node.UpdateKey", keyValue, dummy)
// 			client.Close()
// 		}
// 	}

// 	return err
// }

// func (node *Node) ForceDeleteKey(key string, dummy *int) error {

// 	node.nodelock.Lock()
// 	node.logger.Printf("f=ForceDeleteKey, key:%s\n",key)
// 	// fmt.Println("(Replication) Deleted key =", key)
// 	delete(node.KeyValueStore, key)
// 	node.nodelock.Unlock()

// 	return nil
// }

// func (node *Node) DeleteKey(key string, dummy *int) error {
// 	// fmt.Println("Delete request came for key =", key)
// 	var err error
// 	var client *rpc.Client
// 	err = nil

// 	hash := utils.Hash(key)

// 	node.nodelock.Lock()
// 	if index := node.inRange(hash); index>=0 {
// 		if index==0 {	
// 			node.logger.Printf("f=DeleteKey, key=%s\n",key)
// 			delete(node.KeyValueStore,key)

// 			var msg Msg;
// 			msg.Msg = make([]string,1)
// 			msg.Msg[0] = key
// 			msg.Address = node.Address
// 			node.queuelock.Lock()
// 			for i := 0 ; i<constants.ReplicationFactor-1 ;i++ {
// 				if strings.Compare(node.Successors[i], node.Address) == 0 {
// 					break
// 				}
// 				msg.Address = node.Successors[i]
// 				node.MsgQueue.pushBack(msg)
// 			}
// 			node.queuelock.Unlock()
// 			node.nodelock.Unlock()
// 		} else {
// 			primaryIp := node.Predecessors[index-1].Address
// 			node.nodelock.Unlock()
// 			client, err = getClient(primaryIp)
// 			if err == nil {
// 				err = client.Call("Node.DeleteKey", key , dummy)
// 				client.Close()
// 			}
// 		}	
// 	} else {
// 		node.nodelock.Unlock()
// 		var targetIp string
// 		node.IpLookUp(hash, &targetIp)
// 		client, err = getClient(targetIp)
// 		if err == nil{
// 			err = client.Call("Node.DeleteKey", key, dummy)
// 			client.Close()
// 		}
// 	}
	
// 	return err
// }

// func (node *Node) ForceUpdateKeyValueStore(keyValueStore map[string]string, dummy *int) error {

// 	node.nodelock.Lock()
// 	for k,v := range keyValueStore {
// 		node.KeyValueStore[k] = v
// 	}
// 	// node.logger.Printf("ForceUpdateKeyValueStore\n")
// 	node.nodelock.Unlock()
// 	fmt.Println("(Recovery) Added key-store:")
// 	fmt.Println(keyValueStore)
// 	return nil
// }

// func (node *Node) updateMsgQueue() {
// 	var dummy int
// 	for {
// 		node.queuelock.Lock()
// 		if node.MsgQueue.Start >= 0 {
// 			front := node.MsgQueue.popFront()	
// 			node.queuelock.Unlock()
// 			client, err := getClient(front.Address)
// 			if err == nil{
// 				if len(front.Msg) == 2 {
// 					err = client.Call("Node.ForceUpdateKey", front.Msg, dummy)
// 				} else {
// 					err = client.Call("Node.ForceDeleteKey", front.Msg[0], dummy)
// 				}
// 				client.Close()
// 			}
// 		} else {
// 			node.queuelock.Unlock()
// 		}
// 	}

// }
// func (node *Node) updateFingerTable() {

// 	// fmt.Println("Periodic Fingure Table Update")
// 	node.nodelock.Lock()
// 	node.FingerTable[0] = node.Successors[0]
// 	node.nodelock.Unlock()
// 	for i := 1; i < 32; i++ {
// 		var target string
// 		node.IpLookUp((node.NodeId + Power2(i)) % Power2(32), &target)
// 		node.nodelock.Lock()
// 		node.FingerTable[i] = target
// 		node.nodelock.Unlock()
// 	}

// }

// func (node *Node) init() {
// 	node.FingerTable = make([]string, 32)
// 	node.Successors = make([]string, constants.ReplicationFactor)
// 	node.Predecessors = make([]Predecessor,constants.ReplicationFactor)
// 	node.KeyValueStore = make(map[string]string)
// 	// node.MsgQueue = make([]Msg,0)
// 	node.MsgQueue.init()
// }

// func (node *Node) Join(addr string, newnode *Node) error {
// 	// TODO Search for node with most need

// 	newnode.init()
// 	// fmt.Println(newnode.Successors)
// 	// fmt.Println(newnode.FingerTable)

// 	node.nodeupdatelock.Lock()	
// 	node.logger.Printf("f=Join, addr=%s\n", addr)
// 	fmt.Println("Joining new node :", addr, "...")

// 	node.nodelock.Lock()
// 	newnode.NodeId = node.NodeId
// 	newnode.StartRange = (node.StartRange + (node.EndRange - node.StartRange + Power2(32))%Power2(32) /2 + 1) % Power2(32)
// 	newnode.EndRange = newnode.NodeId
// 	newnode.Address = addr
	
// 	copy(newnode.Successors, node.Successors)
// 	copy(newnode.Predecessors[1:], node.Predecessors[0:])
// 	newnode.Predecessors[0] = Predecessor{StartRange:node.StartRange, Address:node.Address}
// 	copy(newnode.FingerTable, node.FingerTable)

// 	/* pass finger table */
// 	/* start end range */
	
// 	var temp Node
// 	temp.init()
// 	temp.NodeId = (node.StartRange + (node.EndRange - node.StartRange + Power2(32))%Power2(32) /2 ) % Power2(32)
// 	temp.StartRange = node.StartRange
// 	temp.EndRange = temp.NodeId
// 	copy(temp.FingerTable, node.FingerTable)
// 	copy(temp.Predecessors,node.Predecessors)
// 	copy(temp.Successors[1:], node.Successors[0:])
// 	temp.Successors[0] = addr
// 	node.nodelock.Unlock()

// 	// create a new finger table
// 	var i int

// 	temp.nodelock = sync.Mutex{}
// 	for ; i < 32; i++ {
// 		if newnode.inRange( (temp.NodeId + Power2(i)) % Power2(32) )==-1 {
// 			var target string
// 			temp.IpLookUp( (temp.NodeId + Power2(i)) % Power2(32), &target)
// 			temp.FingerTable[i] = target
// 		}
// 	}

// 	for i = 0; i < 32; i++ {
// 		if newnode.inRange((temp.NodeId + Power2(i))%Power2(32))>=0 {
// 			temp.FingerTable[i] = temp.Successors[0]
// 			// fmt.Println(i,temp.NodeId,temp.NodeId + Power2(i),newnode.NodeId)
// 		}
// 	}
	
// 	node.nodelock.Lock()

// 	node.queuelock.Lock()
// 	n := len(node.MsgQueue.MsgQueue)
// 	var size int
// 	if node.MsgQueue.Start == -1 {
// 		size=0
// 	} else {
// 		size = (node.MsgQueue.End-node.MsgQueue.Start+n)%n+1
// 	}
	
// 	for i := 0; i < size; i++ {
// 		index := (i+node.MsgQueue.Start)%n
// 		if temp.inRange(utils.Hash(node.MsgQueue.MsgQueue[index].Msg[0])) == 0{
// 			if  strings.Compare(node.MsgQueue.MsgQueue[index].Address, node.Successors[constants.ReplicationFactor-1]) == 0 {
// 				temp.MsgQueue.pushBack(node.MsgQueue.MsgQueue[index])
// 			}
// 		} else {
// 			newnode.MsgQueue.pushBack(node.MsgQueue.MsgQueue[index])
// 		}
// 	}
// 	node.MsgQueue = temp.MsgQueue
// 	node.queuelock.Unlock()

// 	/* key val store distributed */
// 	newnode.KeyValueStore = make(map[string]string)
// 	for k, v := range node.KeyValueStore {
// 		if newnode.inRange(utils.Hash(k)) <= 1 {
// 			newnode.KeyValueStore[k] = v
// 		} 
// 	}

// 	for k,_ := range newnode.KeyValueStore {
// 		if newnode.inRange(utils.Hash(k)) == 0 {
// 			delete(node.KeyValueStore, k)	
// 		}
// 	}


// 	// fmt.Println("In join node is temp Successors=",temp.Successors,"FingerTable=",temp.FingerTable)
// 	// fmt.Println("In join node is temp Successors=",newnode.Successors,"FingerTable=",newnode.FingerTable)
// 	node.NodeId = temp.NodeId
// 	node.StartRange = temp.StartRange
// 	node.EndRange = temp.EndRange
// 	node.FingerTable = temp.FingerTable
// 	node.Successors = temp.Successors
// 	node.nodejoined = true
// 	node.nodelock.Unlock()	// keep nodelock from MsgQueue update till StartRange, EndRange update
// 	node.nodeupdatelock.Unlock()	
// 	return nil
// }

// // TODO: handle multiple failures ok
// func (node *Node) UpdateSuccessors(predecessor Node, successors *[]string) error {
// 	node.nodeupdatelock.Lock()	
// 	// fmt.Println("UpdateSuccessors called from ", predecessor.Address);
// 	if predecessor.EndRange < Power2(32) {
// 		//error detected
// 		node.logger.Printf("f=UpdateSucessors message=\"Predecessor failure detected.\", newPredecessor=%s\n",predecessor.Address)	
// 		if constants.ReplicationFactor>1{
// 			var failures int
// 			for failures=0 ;failures<constants.ReplicationFactor-1; failures++ {
// 				if strings.Compare(node.Predecessors[failures].Address, predecessor.Address)==0 {
// 					break
// 				}
// 			}

// 			keyValueStore := make(map[string]string)
// 			node.nodelock.Lock()
// 			for k,v := range node.KeyValueStore {
// 				if index := node.inRange(utils.Hash(k)); index>=1 && index<=failures {
// 					keyValueStore[k]=v
// 				}
// 			}
// 			node.nodelock.Unlock()
				
// 			// farthest to closest predecessor (not anymore :P) and closest to farthest successor
// 			for i:=failures-1; i>=0; i-- {				
// 				node.nodelock.Lock()
// 				succ := node.Successors[constants.ReplicationFactor-2-i]
// 				node.nodelock.Unlock()
// 				// fmt.Println(succ)
// 				if strings.Compare(succ,node.Address) == 0 {
// 					break
// 				}

// 				var dummy int
// 				client,err := getClient(succ)
// 				if err==nil {
// 					err = client.Call("Node.ForceUpdateKeyValueStore",keyValueStore,&dummy)
// 					client.Close()
// 				}
// 			}

// 		}

// 		node.nodelock.Lock()
// 		node.StartRange = (predecessor.EndRange+1)%Power2(32)
// 		node.nodelock.Unlock()
// 	}
	
// 	node.nodelock.Lock()
// 	copy(node.Predecessors[1:],predecessor.Predecessors)
// 	node.Predecessors[0]=Predecessor{StartRange:node.StartRange,Address:predecessor.Address}
// 	for _,pred := range node.Predecessors {		
// 		fmt.Printf("%s, ",pred.Address)
// 	}
// 	fmt.Println("")

// 	*successors = make([]string, constants.ReplicationFactor)
// 	copy(*successors, node.Successors)
// 	copy((*successors)[1:], (*successors)[0:])
// 	(*successors)[0] = node.Address

// 	node.logger.Printf("f=UpdateSuccessors, predecessor.Address=%s, Predecessors=%v\n",predecessor.Address, node.Predecessors)
// 	node.nodelock.Unlock()
// 	node.nodeupdatelock.Unlock()
// 	return nil
// }

// func (node *Node)  DoLeave(reason string, dummy *string) error {
// 	fmt.Printf("Node Leave executed due to %s...\n", reason)
// 	os.Exit(0)
// 	return nil
// }


// //TODO handle multiple failures ok
// func (node *Node) periodicUpdater() {

// 	for true {

// 		for i := 0; i < constants.ReplicationFactor; i++ {
// 			node.nodeupdatelock.Lock()
// 			// fmt.Println("periodicUpdater running...")
// 			node.nodelock.Lock()
// 			if node.nodejoined {	// after new node joined need to start afresh with i=0 
// 				i = 0
// 				node.nodejoined = false
// 			}
// 			succ := node.Successors[i]
// 			node.nodelock.Unlock()
// 			// fmt.Println("Contacting ",succ,"at index",i)
// 			if strings.Compare(succ,node.Address)==0 {
// 				node.StartRange = (node.EndRange+1)%Power2(32)
// 				node.nodeupdatelock.Unlock()	
// 				break
// 			}

			
// 			client, err := getClient(succ)
// 			if err == nil{
				
// 				succs := make([]string, constants.ReplicationFactor)
				
// 				var tmpnode Node
// 				tmpnode.Predecessors = make([]Predecessor,constants.ReplicationFactor)

// 				node.nodelock.Lock()
// 				tmpnode.Address = node.Address
// 				copy(tmpnode.Predecessors,node.Predecessors)
// 				tmpnode.StartRange = node.StartRange
// 				if i > 0 {
// 					//successor failed
// 					node.logger.Printf("f=periodicUpdater, message=\"Successor failure detected\", newSuccessor=%s\n", succ)
// 					// fmt.Println("Successor failure detected! Contacting ", succ)
// 					tmpnode.EndRange = node.EndRange
// 				} else {
// 					tmpnode.EndRange = Power2(32)
// 				}
// 				node.nodelock.Unlock()
// 				node.nodeupdatelock.Unlock()

// 				err = client.Call("Node.UpdateSuccessors", tmpnode, &succs)
// 				client.Close()

// 				node.nodeupdatelock.Lock()
// 				node.nodelock.Lock()
// 				if node.nodejoined {	// detected node-join and new-successors-list, right now! handle in next iteration
// 					node.nodelock.Unlock()
// 					node.nodeupdatelock.Unlock()
// 					break
// 				}
// 				node.nodelock.Unlock()
				
// 				// fmt.Println(err)
// 				if err == nil {

// 					keyValueStore := make(map[string]string)
// 					node.nodelock.Lock()
// 					newSuccssors := getListDiff(node.Successors,succs,constants.ReplicationFactor-1)
// 					oldSuccessors := getListDiff(succs, node.Successors, constants.ReplicationFactor-1)

// 					// hold nodelock to stop local updates and hold queuelock to  it.
// 					// hold queuelock to stop any updates being sent (and hence overwriting the new KVS)
// 					// release the nodelock to allow local reads but writes get blocked
// 					// for consistency and respecting Total Order of writes/updates.
// 					for k,v := range node.KeyValueStore {
// 						if node.inRange(utils.Hash(k))==0 {
// 							keyValueStore[k]=v
// 						}
// 					}
// 					node.queuelock.Lock()
// 					// node.nodelock.Unlock()

// 					var tmpQueue MsgQueue
// 					tmpQueue.init()
// 					n := len(node.MsgQueue.MsgQueue)
// 					var size int
// 					if node.MsgQueue.Start == -1 {
// 						size=0
// 					} else {
// 						size = (node.MsgQueue.End-node.MsgQueue.Start+n)%n+1
// 					}
					
// 					for i := 0; i < size; i++ {
// 						index := (i+node.MsgQueue.Start)%n
// 						if _, ok := oldSuccessors[node.MsgQueue.MsgQueue[index].Address]; !ok {
// 							tmpQueue.pushBack(node.MsgQueue.MsgQueue[index])
// 							// fmt.Println(temp.MsgQueue.MsgQueue)	
// 						}					
// 					}
// 					node.MsgQueue = tmpQueue

// 					// fmt.Println(newSuccssors)
// 					for succ,_ := range newSuccssors {
// 						if strings.Compare(succ,node.Address)==0 {
// 							break
// 						}
// 						client, err := getClient(succ)
// 						if err == nil {
// 							var dummy int
// 							fmt.Println("(Recovery) Transfering KeyValueStore to ",succ,"...")
// 							// fmt.Println("(Recovery) KeyValueStore: ",keyValueStore)
// 							err = client.Call("Node.ForceUpdateKeyValueStore",keyValueStore,&dummy)
// 							client.Close()
// 						}
// 					}



// 					// node.nodelock.Lock()
// 					copy(node.Successors, succs)
// 					node.nodelock.Unlock()	
// 					node.queuelock.Unlock()
// 					node.nodeupdatelock.Unlock()	
// 					break
// 				} 
// 			}
// 			node.nodeupdatelock.Unlock()	
// 		}

// 		node.updateFingerTable()
		
// 		node.logger.Printf("f=periodicUpdater, FingerTable=%v, Successors=%v\n", node.FingerTable, node.Successors)
// 		// node.printFingerTable()
// 		// node.printDetails()
// 		time.Sleep(1000 * time.Millisecond)
// 	}
// }



// // helper functions for Node class

// func (node *Node) inRange(key uint64) int {
// 	if inInterval(key,node.StartRange, node.EndRange){
// 		return 0
// 	}
// 	EndRange := (node.StartRange-1+Power2(32))%Power2(32)
// 	for i:=0 ; i<constants.ReplicationFactor-1 ; i++ {
// 		if inInterval(key,node.Predecessors[i].StartRange, EndRange){
// 			return i+1
// 		}
// 		EndRange = (node.Predecessors[i].StartRange-1+Power2(32))%Power2(32)
// 	}
// 	return -1
// }


// func (node *Node) printDetails() {
// 	node.nodelock.Lock()
// 	fmt.Println("Range = [", node.StartRange, ",", node.EndRange, "]")
// 	node.nodelock.Unlock()
// }

// func (node *Node) printFingerTable(){
// 	node.nodelock.Lock()
// 	fmt.Println("------Finger Table------")
// 	for i:=0;i<32;i++ {
// 		fmt.Println((node.NodeId+Power2(i)) % Power2(32), node.FingerTable[i])
// 	}
// 	fmt.Println("-------------------------")
// 	node.nodelock.Unlock()
// }

// // stat functions

// func (node *Node) GetStat(dummy int, stat *Stat) error {	
// 	node.nodelock.Lock()
// 	node.queuelock.Lock()
// 	n := len(node.MsgQueue.MsgQueue)
// 	stat.Qsize = (node.MsgQueue.End-node.MsgQueue.Start+n)%n+1
// 	stat.KVSsize = len(node.KeyValueStore)
// 	stat.StartRange = node.StartRange
// 	stat.EndRange = node.EndRange
// 	node.nodelock.Unlock()
// 	node.queuelock.Unlock()
// 	return nil
// }


// func tcpServer(port string){
// 	addr, err := net.ResolveTCPAddr("tcp", port)
// 	if err != nil {
// 		fmt.Println(err)
// 	}

// 	inbound, err := net.ListenTCP("tcp", addr)
// 	if err != nil {
// 		fmt.Println(err)
// 	}
	
// 	for true {
// 		conn, err := inbound.Accept()
		
// 		if err != nil {
// 			fmt.Println(err)
// 		}

// 		go jsonrpc.ServeConn(conn)

// 	}
// }

// func main() {
// 	/* setup master node */
	
// 	node := new(Node)
// 	node.nodelock = sync.Mutex{}
// 	node.queuelock = sync.Mutex{}
// 	node.nodeupdatelock = sync.Mutex{}
// 	node.logger = log.New(os.Stderr,"",log.Ltime|log.Lmicroseconds)  //Lshortfile to print line no

// 	if(strings.Compare(os.Args[2], "master") == 0) {
// 		node.init()
// 		node.Address = os.Args[1]
// 		node.Successors[0] = node.Address
// 		node.Predecessors[0] = Predecessor{StartRange:0,Address:node.Address}
// 		node.StartRange = 0
// 		node.EndRange = Power2(32) - 1
// 		node.NodeId = Power2(32) - 1
// 		node.nodejoined = false // default
// 		fmt.Println("Key Store initialized...")
// 	} else {
// 		client, err := getClient(os.Args[2])
// 		var newnode Node
// 		newnode.init()

// 		if err == nil {
// 			err = client.Call("Node.Join", os.Args[1], node)
// 			client.Close()
// 		} else {
// 			log.Fatal("Unable to join!");
// 		}
// 		fmt.Println("-----Initial Key Value Store------\n")// ,node.KeyValueStore)
// 	}
	
// 	rpc.Register(node)
// 	rpc.HandleHTTP()
// 	go node.periodicUpdater()
// 	go node.updateMsgQueue()

// 	l, e := net.Listen("tcp", os.Args[1])
// 	if e != nil {
// 		log.Fatal("listen error:", e)
// 	}
// 	fmt.Println("Listening...")
// 	http.Serve(l, nil)
// }