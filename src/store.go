package chord

import (
	"errors"
	"strings"
	"time"

	// "database/sql"

	"constants"
	"utils"
)

type DistributedSharedStore struct {
	/// <summary>
	/// Below two Maps essentially implement a priority-queue (on key=Clock),
	/// which can also be queried to get Message for key=Mid/Message-Id
	/// </summar>
	MessageQueue  Map
	MidToClkMap   map[Mid3Phase]Clock3Phase
	KeyValueStore map[string]string
	Clock         uint64
	ProposedClock uint64

	CommitedMessageQueue Vector
	AbortedMessageQueue  Vector
}

type Request3Phase struct {
	Stage                  int
	KeyValuePair           []string
	Clock                  uint64
	Mid                    uint64
	NodeId                 uint64
	Address                string
	AllNonCoordinatorNodes []string
	CreationTime           time.Time

	done chan bool
}

type Mid3Phase struct {
	Mid    uint64
	NodeId uint64
}

type Clock3Phase struct {
	Clock  uint64
	NodeId uint64
}

func (x Clock3Phase) LessThan(y interface{}) bool {
	return x.Clock < y.(Clock3Phase).Clock || (x.Clock == y.(Clock3Phase).Clock && x.NodeId < y.(Clock3Phase).NodeId)
}

/// <see ref=dsm.DistributedSharedMemory.Read.>
func (node *Node) Read(key string, value *string) error {

	node.logger.Printf("node.Read: starting key=%s ...\n", key)

	var hashedKey uint64 = utils.Hash(key)
	var nodeInfo NodeBasicInfo
	var err = node.Successor(hashedKey, &nodeInfo)
	if err == nil {
		if nodeInfo.NodeId == node.NodeId {
			node.nodelock.Lock()
			valueFromStore := strings.Clone(node.Store.KeyValueStore[key])
			node.nodelock.Unlock()
			*value = valueFromStore
		} else {
			node.logger.Printf("node.Read: Calling node=%s ...\n", nodeInfo.Address)

			return utils.Rpc(nodeInfo.Address, "Node.Read", key, value)
		}
	}

	node.logger.Println("node.Read: exiting ...")

	return err
}

/// <see ref=dsm.DistributedSharedMemory.Write.>
func (node *Node) Write(keyValuePair []string, output *int) error {

	node.logger.Printf("node.Write: starting keyValuePair=[%s, %s] ...\n", keyValuePair[0], keyValuePair[1])

	var hashedKey uint64 = utils.Hash(keyValuePair[0])
	var nodeInfo NodeBasicInfo
	var err = node.Successor(hashedKey, &nodeInfo)
	if err == nil {
		if nodeInfo.NodeId == node.NodeId {
			var request Request3Phase

			node.nodelock.Lock()
			node.Store.Clock++
			request.Clock = node.Store.Clock
			request.NodeId = node.NodeId
			request.Address = node.Address
			request.AllNonCoordinatorNodes = make([]string, len(node.Successors))
			for i, successor := range node.Successors {
				request.AllNonCoordinatorNodes[i] = successor.Address
			}
			node.nodelock.Unlock()

			// PHASE 1
			// All nodes are ready to commit :)

			// PHASE 2
			request.Stage = 2
			request.KeyValuePair = keyValuePair
			request.Mid = utils.GetFreshMid()
			request.CreationTime = time.Now().UTC()
			request.done = make(chan bool)
			var finalClock uint64
			err = node.Execute3Phase(request, &finalClock)

			// PHASE 3
			if err == nil {
				// execute Phase 3 "commit"
				request.Stage = 4
				request.Clock = finalClock
				var ignore uint64
				err = node.Execute3Phase(request, &ignore)

				// wait for message delivery
				<-request.done
			} else {
				// execute Phase 3 "wait to merge"
				node.runTerminationProtocol(request)

				// wait for message delivery
				if result := <-request.done; !result {
					err = errors.New("[Write] Transaction aborted!")
				}
			}
		} else {
			node.logger.Printf("node.Write: Calling node=%s ...\n", nodeInfo.Address)

			return utils.Rpc(nodeInfo.Address, "Node.Write", keyValuePair, output)
		}
	}

	node.logger.Println("node.Write: exiting ...")

	return err
}

/// <see ref=dsm.DistributedSharedMemory.Execute3Phase.>
func (node *Node) Execute3Phase(request Request3Phase, output *uint64) error {
	node.logger.Printf("node.Execute3Phase: starting request.Key=%s, request.Mid=%d, request.Clock=%d, request.Stage=%d...\n", request.KeyValuePair[0], request.Mid, request.Clock, request.Stage)

	var err error
	switch request.Stage {
	///
	/// PHASE 1 (Coordinator): Can "Commit"?
	///
	///
	/// PHASE 1 (Sites): Yes
	///

	///
	/// PHASE 2 (Coordinator): "Prepare to Commit"
	///                        Also send a Proposed Clock
	///
	case 2:
		var mid Mid3Phase = Mid3Phase{Mid: request.Mid, NodeId: request.NodeId}
		var clk Clock3Phase = Clock3Phase{Clock: request.Clock, NodeId: request.NodeId}

		node.nodelock.Lock()
		node.Store.MidToClkMap[mid] = clk
		var message Request3Phase = request
		node.Store.MessageQueue.Insert(clk, &message)
		node.nodelock.Unlock()

		*output = 0
		request.Stage = 3
		var finalClock uint64 = 0
		var errorCount int = 0
		for i := range request.AllNonCoordinatorNodes {
			if err = utils.Rpc(request.AllNonCoordinatorNodes[i], "Node.Execute3Phase", request, output); err == nil {
				finalClock = utils.Max(finalClock, *output)
			} else {
				node.logger.Printf("node.Execute3Phase: PHASE 2, Stage=2, mid=%d err=%s...\n", request.Mid, err)
				errorCount++
				err = nil // clear error after logging
				if errorCount > constants.N-constants.Vc {
					err = errors.New("[Execute3Phase][Phase 2] Less than Vc nodes Ack-ed for 'prepare to commit' .. Wait for 'merge' ")
					break
				}
			}
		}

		*output = finalClock

		break

	///
	/// PHASE 2 (Site): Ack
	///                 Also send back our Proposed Clock
	///
	case 3:
		var mid Mid3Phase = Mid3Phase{Mid: request.Mid, NodeId: request.NodeId}
		var clk Clock3Phase = Clock3Phase{Clock: request.Clock, NodeId: request.NodeId}

		node.nodelock.Lock()
		node.Store.MidToClkMap[mid] = clk
		var message Request3Phase = request
		node.Store.MessageQueue.Insert(clk, &message)
		node.Store.ProposedClock = utils.Max(node.Store.ProposedClock+1, request.Clock)
		var storeProposedClockValue uint64 = node.Store.ProposedClock
		node.nodelock.Unlock()

		*output = storeProposedClockValue

		break

	///
	/// PHASE 3 (Coordinator): Commit
	///                 	   Send Final Clock
	///
	case 4:
		// update the local temporary clock for the mid in local queue
		// wait for all the messages to arrive and then move to phase 3 here only
		// after done with sending phase 3 message check for deliverable messages
		var mid Mid3Phase = Mid3Phase{Mid: request.Mid, NodeId: request.NodeId}

		node.nodelock.Lock()
		var clk Clock3Phase = node.Store.MidToClkMap[mid]
		var message *Request3Phase = node.Store.MessageQueue.Get(clk).(*Request3Phase)
		node.Store.MessageQueue.Delete(clk)

		clk.Clock = utils.Max(clk.Clock, request.Clock)
		node.Store.Clock = utils.Max(node.Store.Clock, clk.Clock)
		message.Clock = clk.Clock
		message.Stage = 5
		node.Store.MidToClkMap[mid] = clk
		node.Store.MessageQueue.Insert(clk, message)
		node.nodelock.Unlock()

		request.Stage = 5
		request.Clock = clk.Clock
		for i := range request.AllNonCoordinatorNodes {
			// what if successors fail or have changed from who sent the request
			// for successor failure - does not matter - send to all as sending "commit"
			// for successor change  - ?
			err = utils.Rpc(request.AllNonCoordinatorNodes[i], "Node.Execute3Phase", request, output)
		}

		// finally deliver the message to this node
		go node.deliverMessagesFromQueue()

		break

	///
	/// PHASE 3 (Site): -- (Execute Commit)
	///					   (Receive Final Clock)
	///
	case 5:
		// update the final ts for the mid and deliver the required message
		var mid Mid3Phase = Mid3Phase{Mid: request.Mid, NodeId: request.NodeId}

		node.nodelock.Lock()
		var clk Clock3Phase = node.Store.MidToClkMap[mid]
		var message *Request3Phase = node.Store.MessageQueue.Get(clk).(*Request3Phase)
		node.Store.MessageQueue.Delete(clk)
		clk.Clock = request.Clock
		message.Clock = request.Clock
		message.Stage = 5
		node.Store.MessageQueue.Insert(clk, message)
		node.nodelock.Unlock()

		// finally deliver the messages in queue front that are deliverable to this node
		go node.deliverMessagesFromQueue()

		break

	///
	/// PHASE 3 (Coordinator): Abort
	///                        (Only performed in Execute3PhaseRecovery)
	///
	case 6:
		var mid Mid3Phase = Mid3Phase{Mid: request.Mid, NodeId: request.NodeId}

		node.nodelock.Lock()
		if clk, midExists := node.Store.MidToClkMap[mid]; midExists {
			var message *Request3Phase = node.Store.MessageQueue.Get(clk).(*Request3Phase)
			node.Store.AbortedMessageQueue.PushBack(message)
			delete(node.Store.MidToClkMap, mid)
			node.Store.MessageQueue.Delete(clk)
			if message.done != nil {
				message.done <- false
			}
		}
		node.nodelock.Unlock()

		*output = 0
		request.Stage = 7
		for i := range request.AllNonCoordinatorNodes {
			err = utils.Rpc(request.AllNonCoordinatorNodes[i], "Node.Execute3Phase", request, output)
		}

		// deliver other unblocked messages to this node
		go node.deliverMessagesFromQueue()

		break

	///
	/// PHASE 3 (Site): -- (Execute Abort)
	///                    (Only performed in Execute3PhaseRecovery)
	///
	case 7:
		var mid Mid3Phase = Mid3Phase{Mid: request.Mid, NodeId: request.NodeId}

		node.nodelock.Lock()
		if clk, midExists := node.Store.MidToClkMap[mid]; midExists {
			var message *Request3Phase = node.Store.MessageQueue.Get(clk).(*Request3Phase)
			node.Store.AbortedMessageQueue.PushBack(message)
			delete(node.Store.MidToClkMap, mid)
			node.Store.MessageQueue.Delete(clk)
			if message.done != nil {
				message.done <- false
			}
		}
		node.nodelock.Unlock()

		// deliver other unblocked messages to this node
		go node.deliverMessagesFromQueue()

		break
	default:
	}

	node.logger.Println("node.Execute3Phase: exiting ...")

	return err
}

// function to deliver messages in MessageQueue that are ready for delivery
func (node *Node) deliverMessagesFromQueue() {
	node.nodelock.Lock()
	var leftMostKey Key
	if leftMostKey = node.Store.MessageQueue.GetLeftMost(); leftMostKey == nil {
		node.nodelock.Unlock()
		return
	}
	var clk Clock3Phase = leftMostKey.(Clock3Phase)
	var message *Request3Phase = node.Store.MessageQueue.Get(clk).(*Request3Phase)
	var mid Mid3Phase
	for message != nil && message.Stage == 5 {
		mid = Mid3Phase{Mid: message.Mid, NodeId: message.NodeId}
		node.Store.MessageQueue.Delete(clk)
		delete(node.Store.MidToClkMap, mid)
		node.Store.Clock = utils.Max(node.Store.Clock, clk.Clock) + 1
		node.Store.KeyValueStore[message.KeyValuePair[0]] = message.KeyValuePair[1]
		node.Store.CommitedMessageQueue.PushBack(message)
		if message.done != nil {
			message.done <- true
		}

		if leftMostKey = node.Store.MessageQueue.GetLeftMost(); leftMostKey == nil {
			message = nil
			break
		}
		clk = leftMostKey.(Clock3Phase)
		message = node.Store.MessageQueue.Get(clk).(*Request3Phase)
	}

	if message != nil {
		// update creation ts and execute 3Phase commit termination protocol
		message.CreationTime = time.Now().UTC()
		go node.runTerminationProtocol(*message)
	}

	node.nodelock.Unlock()
}

func (node *Node) runTerminationProtocol(request Request3Phase) error {
	var duration time.Duration = time.Now().UTC().Sub(request.CreationTime)
	if duration < constants.DelayBeforeStartingTerminationProtocol {
		time.Sleep(constants.DelayBeforeStartingTerminationProtocol - duration)
	}

	var err error = nil
	var output uint64

	request.Stage = 8
	if err = node.Execute3PhaseRecovery(request, &output); err == nil {

		// fix request
		node.nodelock.Lock()
		if request.Address != node.Address {
			for i := range request.AllNonCoordinatorNodes {
				if request.AllNonCoordinatorNodes[i] == node.Address {
					request.AllNonCoordinatorNodes[i] = request.Address
					request.Address = node.Address
					break
				}
			}
		}
		node.nodelock.Unlock()

		// call appropriate stage of 3Phase commit
		if output == constants.Uint64Max {
			request.Stage = 6 // 'abort'

			err = node.Execute3Phase(request, &output)
		} else if output > 0 {
			request.Stage = 4      // 'commit'
			request.Clock = output // final clock

			err = node.Execute3Phase(request, &output)
		} else {
			request.Stage = 2 // 'prepare to commit'

			node.nodelock.Lock()
			node.Store.Clock++
			request.Clock = node.Store.Clock
			node.nodelock.Unlock()

			err = node.Execute3Phase(request, &output)
		}
	}

	return err
}

/// <see ref=dsm.DistributedSharedMemory.Execute3PhaseRecovery.>
func (node *Node) Execute3PhaseRecovery(request Request3Phase, output *uint64) error {
	var err error
	switch request.Stage {
	///
	/// Get local state (Coordinator):
	///
	case 8:
		var finalClock uint64 = 0
		var allNodes []string = append([]string(nil), request.Address)
		allNodes = append(allNodes, request.AllNonCoordinatorNodes...)

		var errorCount int = 0
		request.Stage = 9
		for i := range allNodes {
			// output can be a uint64.MaxValue to represent "abort"
			//               0 to represent "prepare to commit" or "wait"
			//               a uint64 number between 0 and uint64.MaxValue to represent "commit" (value is the finalClock)
			// take max to get final consensus value
			if allNodes[i] == node.Address {
				err = node.Execute3PhaseRecovery(request, output)
				finalClock = utils.Max(finalClock, *output)
			} else if err = utils.Rpc(allNodes[i], "Node.Execute3PhaseRecovery", request, output); err == nil {
				finalClock = utils.Max(finalClock, *output)

				// if the first node to return a success response is not us
				// throw to let the ith node complete 'merge' protocol as Coordinator
				if errorCount == i {
					err = errors.New("[Execute3PhaseRecovery][Get local state] Coordinator found: " + allNodes[i])
					break
				}
			} else {
				node.logger.Printf("node.Execute3PhaseRecovery: Request State, Stage=8, mid=%d err=%s...\n", request.Mid, err)
				errorCount++
				err = nil // clear error after logging

				if errorCount > constants.N-constants.Vc {
					err = errors.New("[Execute3PhaseRecovery][Get local state] Less than Vc nodes reachable .. Block till quorum reached ")
					break
				}
			}
		}

		*output = finalClock

	///
	/// Return local state (Site):
	///
	case 9:
		node.nodelock.Lock()

		// received "commit"
		var n int = node.Store.CommitedMessageQueue.Size()
		for i := 0; i < n; i++ {
			var value interface{} = node.Store.CommitedMessageQueue.ElementAt(i)
			if value != nil && value.(*Request3Phase).Mid == request.Mid && value.(*Request3Phase).NodeId == request.NodeId {
				*output = value.(*Request3Phase).Clock // > 0
				node.nodelock.Unlock()
				return nil
			}
		}

		// received "abort"
		n = node.Store.AbortedMessageQueue.Size()
		for i := 0; i < n; i++ {
			var value interface{} = node.Store.AbortedMessageQueue.ElementAt(i)
			if value != nil && value.(*Request3Phase).Mid == request.Mid && value.(*Request3Phase).NodeId == request.NodeId {
				*output = constants.Uint64Max
				node.nodelock.Unlock()
				return nil
			}
		}

		node.nodelock.Unlock()

		// either received "prepare to commit" (can lookup node.Store.MidToClkMap to confirm)
		// or     in "wait" state (i.e. did not receive Phase 2 request)
		*output = 0

		break
	default:
	}

	return nil
}

/// <see ref=dsm.DistributedSharedMemory.Restore.>
func (node *Node) Restore(start int, output *[]Request3Phase) error {
	var batch []Request3Phase = make([]Request3Phase, constants.RestoreBatchSize)
	node.nodelock.Lock()
	var commitLogSize = node.Store.CommitedMessageQueue.Size()
	for i := 0; start + i < commitLogSize && i < constants.RestoreBatchSize; i++ {
		batch[i] = *node.Store.CommitedMessageQueue.ElementAt(start+i).(*Request3Phase)
	}
	node.nodelock.Unlock()

	for i := utils.MaxInt(commitLogSize - start,0); i < constants.RestoreBatchSize; i++ {
		batch[i].Clock = 0
		batch[i].KeyValuePair = nil
	}

	*output = batch

	return nil
}