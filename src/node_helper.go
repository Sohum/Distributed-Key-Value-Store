package chord

import (
	"constants"
	"utils"
)

type UpdateFingerTableArgument struct {
	NodeInfo NodeBasicInfo
	FingerIndex int 
}

/// <see ref=chord.Chord.join>
func (node *Node) join(nodeBasicInfo NodeBasicInfo) error {
	var err error = nil
	// if <node> is the only node in the ring
	if (node.NodeId == nodeBasicInfo.NodeId) {
		node.NodePredecessor = nil
		node.Successors = make([]NodeBasicInfo, 0)
		node.FingerTable = make([]NodeBasicInfo, constants.FingerTableSize)
		for i:=0; i<constants.FingerTableSize; i++ {
			node.FingerTable[i] = nodeBasicInfo
		}
	} else {
		// section: <node>.init_finger_table(<nodeBasicInfo>)
		var successorInfo NodeBasicInfo
		node.Successors = make([]NodeBasicInfo, 1)
		if err = utils.Rpc(nodeBasicInfo.Address, "Node.Successor", (node.NodeId+1)%constants.Mod, &successorInfo); err != nil {
			return err
		}
		node.Successors[0] = successorInfo
		node.NodePredecessor = new(NodeBasicInfo)
		if err = utils.Rpc(nodeBasicInfo.Address, "Node.Predecessor", node.NodeId, node.NodePredecessor); err != nil {
			return err
		}

		node.logger.Println("node.join: node successor, predecessor info retrieved")

		var nodeInfo NodeBasicInfo
		nodeInfo.NodeId = node.NodeId
		nodeInfo.Address = node.Address
		node.copyPredecessorInto(&nodeInfo)
		node.copySuccessorsInto(&nodeInfo)
		utils.RpcCallAction(successorInfo.Address, "Node.Notify", nodeInfo)

		node.logger.Println("node.join: node successors built")

		node.FingerTable = make([]NodeBasicInfo, constants.FingerTableSize)
		node.FingerTable[0] = node.Successors[0]
		for i:=1; i<constants.FingerTableSize; i++ {
			var fingerKey uint64 = (node.NodeId+utils.Power2(i))%constants.Mod;
			if utils.InIntervalCO(fingerKey, node.NodeId, node.FingerTable[i-1].NodeId) {
				node.FingerTable[i] = node.FingerTable[i-1]
			} else {
				if err = utils.Rpc(nodeBasicInfo.Address, "Node.Successor", fingerKey, &(node.FingerTable[i])); err != nil {
					return err
				}
			}
		}

		node.logger.Println("node.join: fingertable constructed")

		// *****************************************************************************************************
		// *****************************************************************************************************
		// ******** Special case : we should get the data from Successor								********
		// ******** Get data before updating all the other nodes 										********
		// ******** Successor contains all needed data - only slightly more								********
		// ******** NOTE: Since we don't block DB transcations, messages in MessageQueue will be lost	********
		// *****************************************************************************************************
		// *****************************************************************************************************
		var restoreCompleted bool = false
		for start := 0; !restoreCompleted ; start += constants.RestoreBatchSize {
			var output []Request3Phase
			if err = utils.Rpc(successorInfo.Address, "Node.Restore", start, &output); err != nil {
				return err
			}

			for i := 0; i < constants.RestoreBatchSize && !restoreCompleted; i++ {
				if output[i].Clock != 0 && output[i].KeyValuePair != nil {
					node.Store.CommitedMessageQueue.PushBack(&output[i])
					node.Store.KeyValueStore[output[i].KeyValuePair[0]] = node.Store.KeyValueStore[output[i].KeyValuePair[1]]
				} else {
					// if we hit a nil entry in output - we have no more messages to restore
					restoreCompleted = true
				}
			}
		}

		// section: <node>.update_others()
		var predecessor NodeBasicInfo
		nodeInfo.NodeId = node.NodeId
		nodeInfo.Address = node.Address
		node.copyPredecessorInto(&nodeInfo)
		node.copySuccessorsInto(&nodeInfo)
		for i:=0; i<constants.FingerTableSize ; i++ {
			node.logger.Println("node.join: node notifying, i=", i)
			if err = utils.Rpc(nodeBasicInfo.Address, "Node.Predecessor", (node.NodeId - (utils.Power2(i)%constants.Mod) + constants.Mod)%constants.Mod, &predecessor); err != nil {
				// TODO (sodhar) : can this be best effort?
				return err
			} else if predecessor.NodeId != node.NodeId {
				// no need to update self
				node.logger.Printf("node.join: updatefingertable for node=%s\n", predecessor.Address)
				var input UpdateFingerTableArgument
				input.NodeInfo = nodeInfo
				input.FingerIndex = i
				if err = utils.RpcCallAction(predecessor.Address, "Node.UpdateFingerTable", input); err != nil {
					return err
				}
			}
		}
		
		node.logger.Println("node.join: updatefingertable for others done")
	}

	node.logger.Println("node.join: done")
	return err
}

/// <see ref=chord.Chord.UpdateFingerTable>
func (node *Node) UpdateFingerTable(input UpdateFingerTableArgument, output *int) error {
	node.logger.Printf("node.UpdateFingerTable: starting node=%d, fingerindex=%d ...\n", input.NodeInfo.NodeId, input.FingerIndex)

	if node.NodeId == input.NodeInfo.NodeId {
		// return if reached the callee node
		return nil
	}

	var err error = nil
	var i int = input.FingerIndex
	node.nodelock.Lock()
	// *****************************************************************************************************
	// *****************************************************************************************************
	// ******** Special case if FT[i] == node, we may need to update                                ********
	// ******** This special case requires us to do the sanity check that the input node is a       ********
	// ******** possible Successor for  i-th finger 												********
	// *****************************************************************************************************
	// *****************************************************************************************************
	if !utils.InIntervalCO(input.NodeInfo.NodeId, node.NodeId, (node.NodeId+utils.Power2(i))%constants.Mod) {

		node.logger.Printf("node.UpdateFingerTable: attempt to update node=%d, finger=%d ...\n", node.NodeId, i)

		if node.NodeId == node.FingerTable[i].NodeId || utils.InIntervalCO(input.NodeInfo.NodeId, node.NodeId, node.FingerTable[i].NodeId) {

			node.logger.Printf("node.UpdateFingerTable: updated-node=%d, finger=%d ...\n", node.NodeId, i)
	
			// input.nodeinfo is better i-th finger or FT[i] == node
			node.FingerTable[i].NodeId = input.NodeInfo.NodeId
			node.FingerTable[i].Address = input.NodeInfo.Address
			node.FingerTable[i].Predecessor = input.NodeInfo.Predecessor
			node.FingerTable[i].Successors = make([]NodeIdInfo, len(input.NodeInfo.Successors))
			copy(node.FingerTable[i].Successors, input.NodeInfo.Successors)
			
			// FingerTable[0] should match Successors[0]
			if i == 0 {
				node.Successors = InsertToList(node.Successors, node.FingerTable[0], 0, constants.ReplicationFactor)
			}
			
			if node.NodePredecessor != nil {
				node.nodelock.Unlock()
				err = utils.RpcCallAction(node.NodePredecessor.Address, "Node.UpdateFingerTable", input)
				node.nodelock.Lock()
			}
		}
	}

	node.nodelock.Unlock()
	node.logger.Println("node.UpdateFingerTable: exiting ...")
	return err
}

/// function for printing <node> fields for debugging purposes
func (node *Node) prettyPrint() {
	node.nodelock.Lock()
	node.logger.Printf("node.prettyPrint: NodeId=%d, Address=%s\n", node.NodeId, node.Address)
	if node.NodePredecessor != nil {
		node.logger.Printf("node.prettyPrint: NodePredecessor.NodeId=%d, NodePredecessor.Address=%s\n", node.NodePredecessor.NodeId, node.NodePredecessor.Address)
	} else {
		node.logger.Println("node.prettyPrint: NodePredecessor=nil")
	}

	if len(node.Successors) > 0 {
		for i := 0; i <len(node.Successors); i++ {
			node.logger.Printf("node.prettyPrint: Successor[%d]={NodeId=%d, Address=%s}\n", i, node.Successors[i].NodeId, node.Successors[i].Address)
		}
	} else {
		node.logger.Println("node.prettyPrint: Successors=[]")
	}

	if len(node.FingerTable) > 0 {
		for i := 0; i <len(node.FingerTable); i++ {
			node.logger.Printf("node.prettyPrint: FT[%d]={NodeId=%d, Address=%s}\n", i, node.FingerTable[i].NodeId, node.FingerTable[i].Address)
		}
	}
	node.nodelock.Unlock()
}


/// function to copy Predecessor info from <node> into <nodeBasicInfo>
func (node *Node) copyPredecessorInto(nodeBasicInfo *NodeBasicInfo) {
	if node.NodePredecessor != nil {
		nodeBasicInfo.Predecessor = new(NodeIdInfo)
		nodeBasicInfo.Predecessor.NodeId = node.NodePredecessor.NodeId
		nodeBasicInfo.Predecessor.Address = node.NodePredecessor.Address	
	}
}

/// function to copy Successors info from <node> into <nodeBasicInfo>
func (node *Node) copySuccessorsInto(nodeBasicInfo *NodeBasicInfo) {
	nodeBasicInfo.Successors = make([]NodeIdInfo, len(node.Successors))
	for i:=0; i<len(node.Successors); i++ {
		nodeBasicInfo.Successors[i].NodeId = node.Successors[i].NodeId
		nodeBasicInfo.Successors[i].Address = node.Successors[i].Address
	}
}



func InsertToList(list []NodeBasicInfo, item NodeBasicInfo, index int, maxlen int) []NodeBasicInfo {
	var sz int = utils.Min(len(list)+1, maxlen)
	list = append(list, item)
	list = list[:sz]

	for i := sz-1; i>index; i-- {
		list[i] = list[i-1]
	}
	list[index] = item

	return list
}

func RemoveFromList(list []NodeBasicInfo, index int) []NodeBasicInfo {
	var sz int = len(list)
	for i := index; i < sz-1; i++ {
		list[i] = list[i+1]
	}
	return list[:sz-1]
}
