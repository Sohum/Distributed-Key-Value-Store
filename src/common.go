package chord

type Msg struct {
	Address string
	Msg 	[]string
}

type Stat struct {
	Qsize 		int
	KVSsize 	int
	StartRange 	uint64
	EndRange 	uint64
}

/************************************************************************************************
			Vector
			------
*************************************************************************************************/

type Vector struct {
	Array 	[]interface{}
	Start 	int
	End 	int
}

func (vector *Vector) Init() {
	vector.Array = make([]interface{}, 1)
	vector.Start = -1
	vector.End = -1
}

func (vector *Vector) PushBack(element interface{}) {
	n := len(vector.Array)	// >0
	if vector.Start == -1 {
		vector.Array[0] = element
		vector.Start = 0 
		vector.End = 0
	} else if vector.Start == (vector.End+1)%n {
		temp_vector := make([]interface{}, 2*n)
		size := (vector.End-vector.Start+n)%n+1
		for i := 0; i < size; i++ {
			temp_vector[i] = vector.Array[(vector.Start+i)%n]
		}
		temp_vector[size] = element
		vector.Array = temp_vector
		vector.Start = 0
		vector.End = size
	} else {
		vector.End = (vector.End+1)%n
		vector.Array[vector.End] = element
	}
}

func (vector *Vector) PopFront() (element interface{}) {
	n := len(vector.Array)	// > 0
	if vector.Start == -1 {
		return nil
	} else if vector.Start == vector.End {
		element = vector.Array[vector.Start]
		vector.Start = -1
		vector.End = -1
	} else {
		element = vector.Array[vector.Start]
		vector.Start = (vector.Start+1)%n
	}

	size := (vector.End-vector.Start+n)%n+1
	if size <= n/2 && size >= 1 {
		temp_vector := make([]interface{}, n/2)
		for i := 0; i < size; i++ {
			temp_vector[i] = vector.Array[(i+vector.Start)%n]
		}
		vector.Array = temp_vector
		vector.Start = 0
		vector.End = size-1
	}
	return element
}

func (vector *Vector) ElementAt(index int) interface{} {
	n := len(vector.Array) // > 0
	if vector.Start == -1 || index > ((vector.End - vector.Start + n)%n) {
		return nil
	}

	return vector.Array[(vector.Start+index)%n];
}

func (vector *Vector) Size() int {
	if vector.Start == -1 && vector.End == -1 {
		return 0
	}

	var n int = len(vector.Array)
	return (vector.End - vector.Start + n)%n 
}


/************************************************************************************************
			Symmetric binary B-Trees: Data structure and maintenance algorithms (red black trees)
			-----------------------------------------------------------------------

			Rudolf Bayer
			Technical University of Munich
			https://link.springer.com/article/10.1007%2FBF00289509

			src: https://en.wikipedia.org/wiki/Red%E2%80%93black_tree
*************************************************************************************************/
type Key interface {
	LessThan(interface{}) bool
}

type RedBlackTreeNode struct {
	Left *RedBlackTreeNode
	Right *RedBlackTreeNode
	Parent *RedBlackTreeNode
	Key Key
	Value interface{}
	Color bool
}

type Map struct {
	Root *RedBlackTreeNode
}

/// <precondition> key!=nil </precondition>
func (tree *Map) Get(key Key) interface{} {
	var node *RedBlackTreeNode = GetInternal(tree.Root, key)
	if node != nil {
		return node.Value
	}

	return nil
}

func GetInternal(root *RedBlackTreeNode, key Key) *RedBlackTreeNode {
	if root != nil && root.Key.LessThan(key) {
		return GetInternal(root.Right, key)
	} else if root != nil && key.LessThan(root.Key) {
		return GetInternal(root.Left, key)
	} else if root != nil {
		return root
	}

	return nil
}





/// <precondition> key!=nil && value!=nil </precondition>
func (tree *Map) Insert(key Key, value interface{}) bool {
	var treeNode *RedBlackTreeNode = &RedBlackTreeNode{Key: key, Value: value, Color: false}
	var insertedNewNode bool = InsertInternal(tree.Root, treeNode)
	if insertedNewNode {
		RepairTreeInsert(treeNode)
		for treeNode.Parent != nil {
			treeNode = treeNode.Parent
		}
		tree.Root = treeNode
	}
	return insertedNewNode
}

/// <precondition> treeNode!=nil </precondition>
func InsertInternal(root *RedBlackTreeNode, treeNode *RedBlackTreeNode) bool {
	if root != nil && treeNode.Key.LessThan(root.Key) {
		if root.Left != nil {
			return InsertInternal(root.Left, treeNode)
		} else {
			root.Left = treeNode
			treeNode.Parent = root
			return true
		}
	} else if root != nil && root.Key.LessThan(treeNode.Key) {
		if root.Right != nil {
			return InsertInternal(root.Right, treeNode)
		} else {
			root.Right = treeNode
			treeNode.Parent = root
			return true
		}
	} else if root != nil {
		root.Value = treeNode.Value
		return false
	}
	return true
}

/// <precondition> treeNode!=nil </precondition>
func RepairTreeInsert(treeNode *RedBlackTreeNode) {
	var uncle, parent, grandParent *RedBlackTreeNode
	parent = treeNode.Parent
	if parent != nil && parent.Parent != nil {
		grandParent = parent.Parent
		if parent != grandParent.Left {
			uncle = grandParent.Left
		} else {
			uncle = grandParent.Right
		}
	}

	if parent == nil {
		treeNode.Color = true
		return
	} else if parent.Color == true {
		return
	} else if uncle != nil && uncle.Color == false {
		uncle.Color = true
		parent.Color = true
		grandParent.Color = false
		RepairTreeInsert(grandParent)
	} else {
		if (treeNode == parent.Left && grandParent != nil && grandParent.Right == parent) || (treeNode == parent.Right && grandParent != nil && grandParent.Left == parent) {
			Rotate(treeNode)
			parent = treeNode
		}
		if grandParent != nil {
			grandParent.Color = false
			Rotate(parent)
		}
		parent.Color = true
	}
}





/// <precondition> treeNode!=nil </precondition>
func Rotate(treeNode *RedBlackTreeNode) {
	var parent, grandParent *RedBlackTreeNode
	parent = treeNode.Parent
	if parent == nil {
		return
	}
	grandParent = parent.Parent 

	treeNode.Parent = grandParent
	if grandParent != nil && parent == grandParent.Left {
		grandParent.Left = treeNode
	} else if grandParent != nil && parent == grandParent.Right {
		grandParent.Right = treeNode
	}

	parent.Parent = treeNode
	if treeNode == parent.Left {
		parent.Left = treeNode.Right
		if treeNode.Right != nil {
			treeNode.Right.Parent = parent 
		}
		treeNode.Right = parent
	} else {
		parent.Right = treeNode.Left
		if treeNode.Left != nil {
			treeNode.Left.Parent = parent 
		}
		treeNode.Left = parent
	}
} 





func (tree *Map) Delete(key Key) bool {
	var deletedNode *RedBlackTreeNode = DeleteInternal(tree.Root, key)
	if deletedNode != nil && deletedNode.Color == true {
		if deletedNode.Left != nil {
			RepairTreeDelete(deletedNode.Left)
		} else if deletedNode.Right != nil {
			RepairTreeDelete(deletedNode.Right)
		} else if deletedNode.Parent != nil {
			var leafNode *RedBlackTreeNode = &RedBlackTreeNode{Parent: deletedNode.Parent, Color: true}
			if deletedNode.Parent.Left == nil {
				deletedNode.Parent.Left = leafNode
			} else if deletedNode.Parent.Right == nil {
				deletedNode.Parent.Right = leafNode
			} else {
				// only root present
				return true
			}
			RepairTreeDelete(leafNode)
			// fmt.Println("after deleting leaf")
			//Print(tree.Root)
			// fmt.Println("")
			if leafNode.Parent.Left == leafNode {
				leafNode.Parent.Left = nil
			} else if leafNode.Parent.Right == leafNode {
				leafNode.Parent.Right = nil				
			} else {
				// fmt.Println("haha3")				
			}
			// fmt.Println("haha2", deletedNode != nil)
		}
	}
	if deletedNode == tree.Root {
		tree.Root = nil
	} else {
		for tree.Root.Parent != nil {
			tree.Root = tree.Root.Parent
		} 
	}
	return deletedNode != nil
}

/// <precondition> treeNode!=nil </precondition>
func DeleteInternal(root *RedBlackTreeNode, key Key) *RedBlackTreeNode {
	var node, keyNode, parent, child *RedBlackTreeNode
	keyNode = GetInternal(root, key)
	if keyNode == nil {
		// fmt.Println("not found key=", key)		
		return nil
	}
	// fmt.Println("keyNode=", keyNode.Parent, keyNode.Left,keyNode.Right)

	node = keyNode
	if node.Right != nil {
		node = node.Right
		for node.Left != nil {
			node = node.Left
		}
		child = node.Right
	} else if node.Left != nil {
		node = node.Left
		for node.Right != nil {
			node = node.Right
		}
		child = node.Left
	}

	parent = node.Parent
	if parent != nil && parent.Left == node {
		parent.Left = child
	} else if parent != nil {
		parent.Right = child
	}

	if child != nil {
		child.Parent = parent
	}

	keyNode.Key = node.Key
	keyNode.Value = node.Value
	// fmt.Println("paerent, child = ", parent, child)
	return node
}

/// <precondition> treeNode!=nil && tree(treeNode) has one less black node now </precondition>
func RepairTreeDelete(treeNode *RedBlackTreeNode) {
	var parent, sibling *RedBlackTreeNode

	parent = treeNode.Parent
	sibling = GetSibling(treeNode)

	if treeNode.Color == false {
		treeNode.Color = true
		return
	} else if parent != nil {
		// make sibling black
		if sibling != nil && sibling.Color == false {
			parent.Color = false
			sibling.Color = true
			Rotate(sibling)
			sibling = GetSibling(treeNode)
		}

		// sibling != nil as the other branch has a non-leaf black node
		if (sibling.Left == nil || sibling.Left.Color == true) && (sibling.Right == nil || sibling.Right.Color == true) {
			sibling.Color = false
			RepairTreeDelete(parent)
			return
		}
		// make sure the sibling's farther child is red if present
		if treeNode == parent.Left && sibling.Right != nil && sibling.Right.Color == true {
			sibling.Color = false
			sibling.Left.Color = true // is not null
			Rotate(sibling.Left)
			sibling = sibling.Left
		} else if treeNode == parent.Right && sibling.Left != nil && sibling.Left.Color == true {
			sibling.Color = false
			sibling.Right.Color = true // is not null
			Rotate(sibling.Right)
			sibling = sibling.Right
		}
		// rotate left to increase black nodes on left side
		sibling.Color = parent.Color
		parent.Color = true
		Rotate(sibling)
		// mark sibling's farthest child black to balance black nodes on right side
		if sibling.Left == parent && sibling.Right != nil {
			sibling.Right.Color = true
		} else if sibling.Right == parent && sibling.Left != nil {
			sibling.Left.Color = true
		}
	}
}

/// <precondition> treeNode!=nil </precondition>
func GetSibling(treeNode *RedBlackTreeNode) *RedBlackTreeNode {
	var parent *RedBlackTreeNode = treeNode.Parent
	if parent != nil && treeNode == parent.Left {
		return parent.Right
	} else if parent != nil && treeNode == parent.Left {
		return parent.Left
	} 
	return nil
}





func (tree *Map) GetLeftMost() Key {
	var leftMostNode *RedBlackTreeNode = GetLeftMostInternal(tree.Root)
	if leftMostNode != nil {
		return leftMostNode.Key
	}

	return nil
}

/// <precondition> treeNode!=nil </precondition>
func GetLeftMostInternal(treeNode *RedBlackTreeNode) *RedBlackTreeNode {
	if treeNode != nil && treeNode.Left != nil {
		return GetLeftMostInternal(treeNode.Left)
	}

	return treeNode
}