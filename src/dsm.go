package chord

/*******************************************************************************************
			Distributed Shared Memory: Sequentially consistent
			-----------------------------------------------------------------------
						Ajay Kshemkalyani and Mukesh Singhal
							Cambridge University

			src: https://www.cs.uic.edu/~ajayk/Chapter12.pdf (DSM)
			src: https://www.cs.uic.edu/~ajayk/Chapter6.pdf (message ordering)

			src: https://ecommons.cornell.edu/bitstream/handle/1813/6323/82-483.pdf?sequence=1&isAllowed=y (3 phase commit)
*******************************************************************************************/

type DistributedSharedMemory interface {
	/// <summary>
	/// read <value> for <key> from the distributed shared memory
	/// </summary>
	/// <remarks> 
	/// query <node> storing value for <key>
	/// sequential consistency allows local reads
	/// </remarks>
	Read(key string, value *string) error;
	
	/// <summary>
	/// write <value> for <key> to the distributed shared memory
	/// </summary>
	/// <remarks> 
	/// query <node> that is expected to store value for <key>
	/// sequential consistency requires totally-ordered write operations
	/// we use 3-phase algorithm for FIFO + T.O. message ordering
	/// </remarks>
	Write(keyValuePair [2]string, output *int) error;

	/// <summary>
	/// 3-phase commit algorithm, with FIFO + T.O. message ordering
	/// </summary>	
	Execute3Phase(request Request3Phase, output *uint64) error;

	/// <summary>
	/// 3-phase commit algorithm - recovery
	/// implements termination and merging protocol
	/// </summary>
	Execute3PhaseRecovery(request Request3Phase, output *uint64) error;

	/// <summary>
	/// Restore distributed shared memory instance
	/// start from <start> index in the Commit-log and send <len(output)> entries 
	/// </summary>
	Restore(start int, output []Request3Phase) error;
}