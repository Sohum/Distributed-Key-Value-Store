package chord

/*******************************************************************************************
			Chord: A Scalable Peer-to-peer Lookup Service for Internet Applications
			-----------------------------------------------------------------------

			Ion Stoica, Robert Morris, David Karger, M. Frans Kaashoek, Hari Balakrishnan 
								MIT Laboratory for Computer Science
										chord@lcs.mit.edu
									http://pdos.lcs.mit.edu/chord

			src: https://pdos.csail.mit.edu/papers/chord:sigcomm01/chord_sigcomm.pdf
*******************************************************************************************/

type Chord interface {
	/// <summary>
	/// periodically verify current <node>'s immediate successors
	/// and notify the successors about the presence of <node> 
	/// 
	/// *** as an extension we also maintain predecessors and when 
	/// *** we detect failure we do replication/KVS recovery
	/// </summary>
	/// <remarks>
	/// *** pull model or recovery for handling multi-failure scenarios 
	/// </remarks>
	stabilize();

	/// <summary>
	///  <caller> thinks it might be one of our immediate predecessors 
	/// </summary>
	Notify(caller Node, output *int) error;

	/// <summary>
	/// periodically verify current <node>'s immediate successors
	/// and notify the successors about the presence of <node> 
	/// </summary>
	fixFingers();



	/// <summary>
	/// setup <node> to join the network/ring (a one-time operation)
	/// <nodeBasicInfo> denotes an arbitrary node in the network/ring
	/// </summary>
	/// <remarks>
	/// *** pull model or recovery for handling multi-failure scenarios 
	/// </remarks>
	join(nodeBasicInfo NodeBasicInfo);

	/// <summary>
	/// if <s> is <i>-th finger of <node>, update <node>'s fingerTable with <s>
	/// where s := input.NodeInfo and i := input.FingerIndex
	/// </summary>
	UpdateFingerTable(argument UpdateFingerTableArgument, output *int) error;



	/// <summary>
	/// query asking for the successor for <key> in the consistent hash ring
	/// </summary>
	/// <remarks>
	/// find the right predecessor (easier to do) and the predecessor knows 
	/// the right successor
	/// </remarks>
	Successor(key uint64, successorInfo *NodeBasicInfo) error;

	/// <summary>
	/// query asking for the predecessor for <key> in the consistent hash ring
	/// </summary>
	/// <remarks>
	/// used in Successor(key, successorInfo)
	/// </remarks>
	Predecessor(key uint64, predecessorInfo *NodeBasicInfo) error;


	/// ###### Helper APIs ######

	/// <summary>
	/// query asking closest preceeding finger for <key> in the consistent hash ring
	/// </summary>
	/// <remarks>
	/// called only when the <node> is not the closest preceeding finger
	/// </remarks>
	GetClosestPreceedingFinger(key uint64, finger *NodeBasicInfo) error;

}
