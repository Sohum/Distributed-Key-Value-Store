package chord

// import (
// 	"utils"
// 	"constants"
// )

func (node *Node) DisasterRecovery(failedNode NodeBasicInfo, output *int) error {
	// node.FailureBroadcast(failedNode, output)
	if *output == 0 {
		// recovery for KVS
	}

	return nil
}

// func (node *Node) FailureBroadcast(failedNode NodeBasicInfo, output *int) error {
// 	// fingertable while async rpc calls are made
// 	var neighbours map[string]bool = make(map[string]bool)
// 	var nodeBasicInfo NodeBasicInfo
// 	*output = 0

// 	node.nodelock.Lock()
// 	var successors, predecessors, callees []NodeBasicInfo
// 	successors = make([]NodeBasicInfo)
// 	for _, successor := range node.Successors {
// 		if successor.NodeId != failedNode.NodeId {
// 			successors = append(successors, successor)
// 		}
// 	}
// 	node.Successors = successors
// 	predecessors = make([]NodeBasicInfo)
// 	for _, predecessor := range node.Predecessors {
// 		if predecessor.NodeId != failedNode.NodeId {
// 			predecessors = append(predecessors, predecessor)
// 		}
// 	}
// 	node.Predecessors = predecessors

// 	for i := 0; i < len(node.FingerTable); i++ {
// 		if node.FingerTable[i].NodeId == failedNode.NodeId {
// 			if i == 0 {
// 				callees = node.Successors
// 				node.nodelock.Unlock()
// 				for _, callee := range callees {
// 					if utils.Rpc(callee.Address, "Node.Successor", callee.NodeId, &nodeBasicInfo) == nil {
// 						break
// 					}
// 				}
// 				node.nodelock.Lock()
// 				node.FingerTable[i] = nodeBasicInfo			
// 			} else {
// 				node.FingerTable[i] = NodeBasicInfo{
// 					NodeId: node.FingerTable[i-1].NodeId,
// 					Address: node.FingerTable[i-1].Address,
// 					Predecessors: node.FingerTable[i-1].Predecessors,
// 					Successors: node.FingerTable[i-1].Successors}				
// 			}
// 		} else {
// 			neighbours[node.FingerTable[i].Address] = true
// 		}
// 	}
// 	node.nodelock.Unlock()

// 	for address, _ := range neighbours {
// 		// errors detected here will also be detected by suitable stabilize() call
// 		utils.Rpc(address, "Node.DisasterRecovery", failedNode, output)
// 	}

// 	for m := 0; m < len(node.FingerTable); m++ {
// 		var fingerInfo NodeBasicInfo
// 		var err error = node.Successor((node.NodeId+utils.Power2(m))%constants.Mod, &fingerInfo)
// 		if err == nil {
// 			node.nodelock.Lock()
// 			node.FingerTable[m] = fingerInfo
// 			node.nodelock.Unlock()
// 		}
// 	}
// }