package utils

import (
	"crypto/sha256"
	"math/rand"
	"net/rpc"
	"strconv"
	"sync"
	"time"

	"constants"
)

// global instance :(
// no concept of static variables in golang; only closures
var connectionPoolLock sync.Mutex = sync.Mutex{}
var connectionPool map[string]*rpc.Client = make(map[string]*rpc.Client)

type Call func() error

func Hash(s string) uint64 {
	var digest [32]byte = sha256.Sum256([]byte(s))
	var hash uint64 = 0
	var intermediate uint64 = 0

	for i, byteVal := range digest {
		intermediate = intermediate*256 + uint64(byteVal)
		if (i+1)%8 == 0 {
			hash ^= intermediate
			intermediate = 0
		}
	}

	return hash
}

func RpcHandleLocal(ipAddress string, method string, input interface{}, output interface{}, localIpAddress string, localCall Call) error {
	if ipAddress == localIpAddress {
		return localCall()
	}

	return Rpc(ipAddress, method, input, output)
}

func Rpc(ipAddress string, method string, input interface{}, output interface{}) error {
	var client *rpc.Client
	var err error
	var foundClient bool

	connectionPoolLock.Lock()
	if client, foundClient = connectionPool[ipAddress]; !foundClient {
		connectionPoolLock.Unlock()
		client, err = rpc.DialHTTP("tcp", ipAddress)
		var numRetries int = 0
		for err != nil && numRetries < constants.MaxRetries {
			// fmt.Println("error in rpc call", err)
			numRetries++
			client, err = rpc.DialHTTP("tcp", ipAddress)
		}

		connectionPoolLock.Lock()
		if err == nil {
			connectionPool[ipAddress] = client
		}
	}
	connectionPoolLock.Unlock()

	if err == nil {
		err = client.Call(method, input, output)

		// if the connections are shut down clean up close client
		if err == rpc.ErrShutdown {
			connectionPoolLock.Lock()
			delete(connectionPool, ipAddress)
			connectionPoolLock.Unlock()
		}
	}

	return err
}

func RpcWithBackoff(ipAddress string, method string, input interface{}, output interface{}) error {
	// TODO (sodhar): ADD BACKOFF AND DELAYS
	var client *rpc.Client
	var err error
	var foundClient bool

	connectionPoolLock.Lock()
	if client, foundClient = connectionPool[ipAddress]; !foundClient {
		connectionPoolLock.Unlock()
		client, err = rpc.DialHTTP("tcp", ipAddress)
		var numRetries int = 0
		for err != nil && numRetries < constants.MaxRetries {
			// fmt.Println("error in rpc call", err)
			numRetries++
			client, err = rpc.DialHTTP("tcp", ipAddress)
		}

		connectionPoolLock.Lock()
		if err == nil {
			connectionPool[ipAddress] = client
		}
	}
	connectionPoolLock.Unlock()

	if err == nil {
		err = client.Call(method, input, output)

		// if the connections are shut down clean up close client
		if err == rpc.ErrShutdown {
			connectionPoolLock.Lock()
			delete(connectionPool, ipAddress)
			connectionPoolLock.Unlock()
		}
	}

	return err
}

func RpcCallActionHandleLocal(ipAddress string, method string, input interface{}, localIpAddress string, localCall Call) error {
	if ipAddress == localIpAddress {
		return localCall()
	}

	return RpcCallAction(ipAddress, method, input)
}

func RpcCallAction(ipAddress string, method string, input interface{}) error {
	var output int
	return Rpc(ipAddress, method, input, &output)
}

func SchedulePeriodicJob(job Call, period time.Duration) {
	var ticker *time.Ticker = time.NewTicker(period)
	for {
		job()
		<-ticker.C
	}
}

// ######## Math related ########

func Power2(power int) uint64 {
	return (uint64(1) << uint(power))
}

func Min(x int, y int) int {
	if x < y {
		return x
	}

	return y
}

func Max(x uint64, y uint64) uint64 {
	if x < y {
		return y
	}

	return x
}

func MaxInt(x int, y int) int {
	if x < y {
		return y
	}

	return x
}

func InInterval(key uint64, begin uint64, end uint64) bool {
	if begin <= end {
		return begin <= key && key <= end
	} else {
		return begin <= key || key <= end
	}
}

func InIntervalOO(key uint64, begin uint64, end uint64) bool {
	return InInterval(key, begin, end) && key != begin && key != end
}

func InIntervalOC(key uint64, begin uint64, end uint64) bool {
	return InInterval(key, begin, end) && key != begin
}

func InIntervalCO(key uint64, begin uint64, end uint64) bool {
	return InInterval(key, begin, end) && key != end
}

func InIntervalCC(key uint64, begin uint64, end uint64) bool {
	return InInterval(key, begin, end)
}

func GetFreshMid() uint64 {
	return (uint64)(time.Now().UnixNano())
}

/// ###### Helpers #######

func RandomString() string {
	return strconv.Itoa(rand.Intn(1000000000))
}

func RandomInt(minval int, maxval int) int {
	// TODO (sodhar): Throw if minval>=maxval
	return minval + rand.Intn(maxval-minval)
}

func GetListDiff(old []string, neW []string, num int) map[string]bool {
	olds := make(map[string]bool)
	neWs := make(map[string]bool)
	diff := make(map[string]bool)

	for i := 0; i < num; i++ {
		olds[old[i]] = true
	}

	for i := 0; i < num; i++ {
		neWs[neW[i]] = true
	}

	for k, _ := range neWs {
		if !olds[k] {
			diff[k] = true
		}
	}

	return diff
}
