package main

import (
	"fmt"
	"strings"
	"strconv"
	"bufio"
	"os"
)

type Inconsistency struct {
	line string
	key string
	oldval, newval int
} 

func main(){
	mapper := make(map[string]int)
	buffer := bufio.NewReader(os.Stdin)
	var inconsistencies []Inconsistency
	for true {
		line, err := buffer.ReadString('\n')
		if err != nil {
			fmt.Println(err)
			break
		}
		split := strings.Split(line, ",")

		if len(split)<3 {
			continue
		}

		split[0] = strings.Split(split[0], " ")[1]	
		// fmt.Println(split[0])
		if strings.Compare(split[0], "f=UpdateKey") == 0 || strings.Compare(split[0], "f=ForceUpdateKey") == 0 {
			key := split[1][5:]
			svalue := strings.Trim(split[2][7:]," \n")
			value, err := strconv.Atoi(svalue)
			// fmt.Println("checking ... value=",svalue,"...");
			if err!=nil {
				continue				
			}
			if v, ok := mapper[key]; ok {
				if v > value {
					// fmt.Printf("old-value=%d new-value=%d line=\"%s\" key=%s\n", v, value, line, key)
					inconsistencies = append(inconsistencies, Inconsistency{line:line, oldval:v, newval:value, key:key})
				}
			}
			mapper[key] = value	
		}
	}
	if len(inconsistencies)>0 {
		fmt.Println("Not Sequentially Consistent")
		for _, v := range inconsistencies {
			fmt.Println("key=", v.key, "oldval=", v.oldval, "newval=", v.newval)
			// fmt.Println(inconsistencies)
		}
	}
}