package main

import (
	"fmt"

	"chord"
)

func Print(root *chord.RedBlackTreeNode) {
	if root == nil {
		return
	}
	fmt.Print("(")
	Print(root.Left)
	fmt.Print(root.Key, root.Color)
	Print(root.Right)
	fmt.Print(")")
}

type KeyI struct {
	val string
}

func (k KeyI) LessThan(k2 interface{}) bool {
	return k.val < k2.(KeyI).val
}

func main() {
	tree := new(chord.Map)
	tree.Insert(KeyI{val: "1"}, "1")
	tree.Insert(KeyI{val: "2"}, "2")
	tree.Insert(KeyI{val: "3"}, "3")
	tree.Insert(KeyI{val: "4"}, "4")
	tree.Insert(KeyI{val: "5"}, "5")
	tree.Insert(KeyI{val: "6"}, "6")
	tree.Insert(KeyI{val: "7"}, "7")
	tree.Insert(KeyI{val: "8"}, "8")
	fmt.Println(tree.Get(KeyI{val: "1"}))
	fmt.Println(tree.Get(KeyI{val: "2"}))
	fmt.Println(tree.Get(KeyI{val: "3"}))
	fmt.Println(tree.Get(KeyI{val: "4"}))
	fmt.Println(tree.Get(KeyI{val: "5"}))
	fmt.Println(tree.Get(KeyI{val: "6"}))
	fmt.Println(tree.Get(KeyI{val: "7"}))
	fmt.Println(tree.Get(KeyI{val: "8"}))
	Print(tree.Root)
	fmt.Println("")
	tree.Delete(KeyI{val: "1"})
	Print(tree.Root)
	fmt.Println("")
	fmt.Println(nil == tree.Get(KeyI{val: "1"}))
	fmt.Println(tree.Get(KeyI{val: "2"}))
	fmt.Println(tree.Get(KeyI{val: "3"}))
	fmt.Println(tree.Get(KeyI{val: "4"}))
	fmt.Println(tree.Get(KeyI{val: "5"}))
	fmt.Println(tree.Get(KeyI{val: "6"}))
	fmt.Println(tree.Get(KeyI{val: "7"}))
	fmt.Println(tree.Get(KeyI{val: "8"}))

	Print(tree.Root)
	fmt.Println("")
	tree.Delete(KeyI{val: "2"})
	Print(tree.Root)
	fmt.Println("")
	fmt.Println(nil == tree.Get(KeyI{val: "2"}))
	fmt.Println(tree.Get(KeyI{val: "3"}))
	fmt.Println(tree.Get(KeyI{val: "4"}))
	fmt.Println(tree.Get(KeyI{val: "5"}))
	fmt.Println(tree.Get(KeyI{val: "6"}))
	fmt.Println(tree.Get(KeyI{val: "7"}))
	fmt.Println(tree.Get(KeyI{val: "8"}))

	Print(tree.Root)
	fmt.Println("")
	tree.Delete(KeyI{val: "3"})
	Print(tree.Root)
	fmt.Println("")
	fmt.Println(nil == tree.Get(KeyI{val: "3"}))
	fmt.Println(tree.Get(KeyI{val: "4"}))
	fmt.Println(tree.Get(KeyI{val: "5"}))
	fmt.Println(tree.Get(KeyI{val: "6"}))
	fmt.Println(tree.Get(KeyI{val: "7"}))
	fmt.Println(tree.Get(KeyI{val: "8"}))

	Print(tree.Root)
	fmt.Println("")
	tree.Delete(KeyI{val: "4"})
	Print(tree.Root)
	fmt.Println("")
	fmt.Println(nil == tree.Get(KeyI{val: "4"}))
	fmt.Println(tree.Get(KeyI{val: "5"}))
	fmt.Println(tree.Get(KeyI{val: "6"}))
	fmt.Println(tree.Get(KeyI{val: "7"}))
	fmt.Println(tree.Get(KeyI{val: "8"}))

	Print(tree.Root)
	fmt.Println("")
	tree.Delete(KeyI{val: "5"})
	Print(tree.Root)
	fmt.Println("")
	fmt.Println(nil == tree.Get(KeyI{val: "5"}))
	fmt.Println(tree.Get(KeyI{val: "6"}))
	fmt.Println(tree.Get(KeyI{val: "7"}))
	fmt.Println(tree.Get(KeyI{val: "8"}))

	Print(tree.Root)
	fmt.Println("")
	tree.Delete(KeyI{val: "6"})
	Print(tree.Root)
	fmt.Println("")
	fmt.Println(nil == tree.Get(KeyI{val: "6"}))
	fmt.Println(tree.Get(KeyI{val: "7"}))
	fmt.Println(tree.Get(KeyI{val: "8"}))

	Print(tree.Root)
	fmt.Println("")
	tree.Delete(KeyI{val: "7"})
	Print(tree.Root)
	fmt.Println("")
	fmt.Println(nil == tree.Get(KeyI{val: "7"}))
	fmt.Println(tree.Get(KeyI{val: "8"}))

	Print(tree.Root)
	fmt.Println("")
	tree.Delete(KeyI{val: "8"})
	Print(tree.Root)
	fmt.Println("")
	fmt.Println(nil == tree.Get(KeyI{val: "8"}))
}
