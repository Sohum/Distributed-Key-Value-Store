echo "starting nodes..."
./node :8000 master 2>0.log >/dev/null &
pid0="$(echo $!)"
sleep 2
./node :8020 :8000 2>20.log >/dev/null &
pid2="$(echo $!)"
sleep 2
./node :8010 :8000 2>10.log >/dev/null &
pid1="$(echo $!)"
sleep 2
./node :8030 :8020 2>30.log >/dev/null &
pid3="$(echo $!)"
sleep 2

echo "phase 1"
./rwriter 3 10 0 :8000 :8010 :8020 :8030

echo "phase 2"
kill -9 "$pid0"
pid0="$pid1"
./node :8015 :8010 2>15.log >/dev/null &
pid1="$(echo $!)"
sleep 5
./rwriter 3 10 500 :8010 :8015 :8020 :8030

echo "phase 3"
kill -9 "$pid2"
pid2="$pid3"
./node :8035 :8030 2>35.log >/dev/null &
pid3="$(echo $!)"
sleep 5
./rwriter 3 10 1000 :8010 :8015 :8030 :8035

pkill node

echo 0.log | ./ct_sniffer
echo 10.log | ./ct_sniffer
echo 20.log | ./ct_sniffer
echo 30.log | ./ct_sniffer
echo 15.log | ./ct_sniffer
echo 35.log | ./ct_sniffer

######################## OLD T.O./CONSISTENCY CHECKING SCRIPT
# function checker()
# {
# 	arr=("$@")
# 	for k in "${arr[@]}"
# 	do
# 		for n in "${nodes[@]}"
# 		do
# 			grep key="$k" "$n".log | awk '{  print $4 }' > "$n".tmp
# 		done
# 		echo "key=$k diff" >> ${nodes[0]}.out
# 		diff "${nodes[0]}".tmp "${nodes[1]}".tmp >> ${nodes[0]}.out
# 		diff "${nodes[0]}".tmp "${nodes[2]}".tmp >> ${nodes[0]}.out	
# 	done
# }
# ./node :8000 master 2>0.log >/dev/null & sleep 2
# ./node :8003 :8000 2>3.log >/dev/null & sleep 2
# ./node :8002 :8000 2>2.log >/dev/null & sleep 2
# ./node :8001 :8000 2>1.log >/dev/null & sleep 2
# ./node :8004 :8003 2>4.log >/dev/null & sleep 2
# ./node :8005 :8004 2>5.log >/dev/null & sleep 2
# ./ct_writer :8000 984059 292790 225447 653891 3090 509107 625356 186258 190364 648553 ; sleep 360
# ./ct_writer :8001 498081 131847 811211 913000 43721 879241 653033 424147 780408 565194 ; sleep 360
# ./ct_writer :8002 203300 410694 128162 341737 902081 830552 889828 272451 649703 86413 ; sleep 360
# ./ct_writer :8003 954425 941318 979947 951957 343133 764324 339106 323237 727887 14376 ; sleep 360
# ./ct_writer :8004 515026 435746 689002 538705 571137 632888 381351 282199 88582 516159 ; sleep 360
# ./ct_writer :8005 138287 828266 240456 122540 431445 989355 903687 298878 511528 960631 ; sleep 360