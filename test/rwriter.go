package main

import (
	"fmt"
	"net/rpc"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"utils"
)

var cnt int64
var lock sync.Mutex
var done chan bool
var iterations int
var keys map[string]bool

// checks consistency if required
func worker(addrs []string, keys []string) {
	var count int64
	count=0
	var dummy int 
	params := make([]string, 3)
	for i:=0; i<iterations; i++ {
		for _, addr := range addrs {
			client, err := rpc.DialHTTP("tcp", addr)
			if err!=nil {
				continue
			}
			for _, k := range keys {
				params[0] = k
				params[2] = utils.RandomString()
				params[1] = params[2]
				err = client.Call("Node.UpdateKey", params , &dummy)
				if err != nil {
					fmt.Println(err)
					continue
				} else {
					count++
				}
				err = client.Call("Node.LookUp", params[0], &params[1])
				if err != nil {
					fmt.Println(err)
				} else if strings.Compare(params[1], params[2])!=0 {
					fmt.Println("Write(key=k, val=v);Read(key=k)==v check failed.")
				} else {
					count++
				}	
			}
		}
	}
	lock.Lock()
	cnt += count
	lock.Unlock()
	done <- true
}


// mode = 0:r 1:rw 2:w 3:onekey(w)
func writer(addr string, mode int64, offset int, check bool) {
	
	client, err := rpc.DialHTTP("tcp", addr)
	if err != nil {
		fmt.Println(err)
		done<-true
		return
	}

	params := make([]string,2)
	var dummy int
	var count int64
	count = 0

	params[0] = utils.RandomString()
	if mode == 3 {
		lock.Lock()
		for _, ok := keys[params[0]]; ok; {
			params[0] = utils.RandomString()	
		}
		keys[params[0]] = true
		lock.Unlock()
	}

	counter := offset
	for i:=0; i<iterations; i++ {	
		if mode!=3 {
			params[0] = utils.RandomString()
		}
		counter++
		params[1] = strconv.Itoa(counter)
					
		if mode>=1 {			
			err = client.Call("Node.UpdateKey", params , &dummy)
			if err != nil {
				fmt.Println(err)
				continue
			} else {
				count++
			}	
		}
		
		if mode<=1 {
			err = client.Call("Node.LookUp",params[0],&params[1])
			if err != nil {
				fmt.Println(err)
				continue
			} else {
				count++
			}			
		}	
	}
	client.Close()
	lock.Lock()
	cnt += count
	lock.Unlock()
	done<-true
}

func main() {
	cnt = 0
	lock = sync.Mutex{}
	keys = make(map[string]bool)

	// n := len(os.Args)
	// mode, _ := strconv.ParseInt(os.Args[1], 10, 64)
	iterations, _ = strconv.Atoi(os.Args[2])
	// offset, _ := strconv.Atoi(os.Args[3])

	done = make(chan bool, 200)
	t1 := time.Now()
	nanos1 := t1.UnixNano()
	for i := 0; i < 200 ; i++ {
		// go writer(os.Args[4+(i%(n-4))], mode, offset)
		ks := make([]string, 10)
		for i:=0; i<10; i++ {
			k := utils.RandomString()
			for _, ok := keys[k]; ok; {
				k = utils.RandomString()
			}
			keys[k] = true
			ks[i] = k
		}
		go worker(os.Args[4:], ks)
	}

	for i:=0; i<200; i++{
		<-done
	}
	t2 := time.Now()
	nanos2 := t2.UnixNano()
	fmt.Println("Throughput=",cnt * 1000000000.0 / (nanos2 - nanos1) )
}