cd ../node
go build
./node 127.0.0.1:8080 initonly 2>/dev/null &
sleep 3
./node 127.0.0.1:8081 127.0.0.1:8080 2>/dev/null &
sleep 3
./node 127.0.0.1:8082 127.0.0.1:8080 2>/dev/null &
sleep 3
./node 127.0.0.1:8083 127.0.0.1:8080 2>/dev/null &
sleep 3
./node 127.0.0.1:8084 127.0.0.1:8080 2>/dev/null &
sleep 3

cd ../client
go build
./client < ../test/test_client_input.txt

pkill node
