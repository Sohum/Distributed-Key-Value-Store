echo "starting nodes..."
./node :8000 master 2>0.log >/dev/null & sleep 2
./node :8002 :8000 2>2.log >/dev/null & sleep 2
./node :8001 :8000 2>1.log >/dev/null & sleep 2
./node :8003 :8002 2>3.log >/dev/null & sleep 2

echo "starting readers/writers..."
echo "Write only throughput"
./rwriter 2 5000 0 :8000 :8001 :8002 :8003
echo "Read only throughput"
./rwriter 0 5000 0 :8000 :8001 :8002 :8003
echo "Read and Write only throughput"
./rwriter 1 5000 0 :8000 :8001 :8002 :8003

pkill node