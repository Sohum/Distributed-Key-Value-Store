package constants

import (
	"time"
)

const MaxRetries int = 5
const ReplicationFactor int = 3
const Mod = (uint64(1) << uint(32))
const FingerTableSize = 32

const Vc int = (ReplicationFactor+1)/2 + 1
const N int = (ReplicationFactor + 1)

const InitOnly string = "initonly"
const StabilizePeriod time.Duration = 30 * time.Second
const DelayBeforeStartingTerminationProtocol time.Duration = 7 * time.Second
const RestoreBatchSize int = 500

const Uint64Max uint64 = ^uint64(0)

type Phase int32

const (
	Phase1Coordinator Phase = iota
	Phase1Site
	Phase2Coordinator
	Phase2Site
	Phase3CoordinatorCommit
	Phase3SiteCommit
	Phase3CoordinatorAbort
	Phase3SiteAbort
)
